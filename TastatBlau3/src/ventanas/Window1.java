package ventanas;
import tastat.*;

import java.awt.EventQueue;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JLabel;
import com.jgoodies.forms.factories.DefaultComponentFactory;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Robot;
import java.awt.Toolkit;

import javax.swing.JSpinner;
import javax.swing.JList;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import javax.swing.text.DocumentFilter.FilterBypass;
import javax.swing.JTextArea;
import tastat.Programa;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.text.AbstractDocument;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.table.DefaultTableModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.SystemColor;
import javax.swing.UIManager;

public class Window1 {

	private JFrame frame;
	private static JTextField idProducteField;
	private static JTextField nomProducteField;
	private static JTextField preuVendaField;
	private static JTextField stockField;
	private static JTextField minStockField;
	private static JTextField proveidorField;
	protected static DefaultTableModel dtm = new DefaultTableModel(new Object[][] {},
			new String[] { "codi producte", "nom producte" });
	private JTable tableComposicio;
	private JTable tableLots;
	static boolean stupido;
	protected Proveidor pr;
	private JTable table;
	private JTable table_1;

	/**
	 * Launch the application.
	 */

	public static void main(Producte p, Magatzem mgz, DefaultTableModel dtm2) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					stupido = true;
					Window1 window = new Window1(p, mgz, dtm2);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public static void main(Magatzem mgz, DefaultTableModel dtm2) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					stupido = true;
					Window1 window = new Window1(mgz, dtm2);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public String selectedButton(ButtonGroup id) {
		for (Enumeration<AbstractButton> buttons = id.getElements(); buttons.hasMoreElements();) {
			AbstractButton button = buttons.nextElement();

			if (button.isSelected()) {
				return button.getText();
			}
		}
		return null;
	}

	/**
	 * Create the application.
	 */

	public Window1(Producte p, Magatzem mgz, DefaultTableModel dtm2) {
		initialize(p, mgz, dtm2);
	}

	public Window1(Magatzem mgz, DefaultTableModel dtm2) {
		Producte p = new Producte("Juan");
		initialize(p, mgz, dtm2);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(Producte p, Magatzem mgz, DefaultTableModel dtm2) {
		while(dtm.getRowCount()>0) dtm.removeRow(0);
		frame = new JFrame();
		frame.setTitle("Productos");
		frame.getContentPane().setForeground(Color.CYAN);
		frame.getContentPane().setBackground(SystemColor.activeCaption);
		frame.setBounds(100, 100, 935, 636);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation(dim.width / 2 - frame.getSize().width / 2, dim.height / 2 - frame.getSize().height / 2);

		JLabel windowTitle = new JLabel(" Producte");
		windowTitle.setBackground(UIManager.getColor("Button.darkShadow"));
		windowTitle.setFont(new Font("Franklin Gothic Medium", Font.PLAIN, 20));
		windowTitle.setBounds(0, 0, 919, 46);
		windowTitle.setOpaque(true);
		frame.getContentPane().add(windowTitle);

		// Tipos ingrediente

		JRadioButton ingredient = new JRadioButton("INGREDIENT");
		ingredient.setBounds(49, 389, 109, 23);
		frame.getContentPane().add(ingredient);

		JRadioButton vendible = new JRadioButton("VENDIBLE");
		vendible.setBounds(49, 415, 109, 23);
		frame.getContentPane().add(vendible);

		// Tipos ingrediente

		JLabel lblTipusDeProducte = new JLabel("Tipus de Producte");
		lblTipusDeProducte.setBounds(49, 368, 92, 14);
		frame.getContentPane().add(lblTipusDeProducte);

		JLabel lblNewJgoodiesLabel = new JLabel("Tipus d'Unitat");
		lblNewJgoodiesLabel.setBounds(241, 368, 92, 14);
		frame.getContentPane().add(lblNewJgoodiesLabel);

		// Tipos unidad

		JRadioButton litre = new JRadioButton("LLITRE");
		litre.setBounds(224, 389, 109, 23);
		frame.getContentPane().add(litre);

		JRadioButton gram = new JRadioButton("GRAM");
		gram.setBounds(224, 415, 109, 23);
		frame.getContentPane().add(gram);

		JRadioButton unitat = new JRadioButton("UNITAT");
		unitat.setBounds(224, 441, 109, 23);
		frame.getContentPane().add(unitat);

		// Tipos unidad

		// Grupo de radio

		ButtonGroup un_tipus = new ButtonGroup();
		un_tipus.add(litre);
		un_tipus.add(gram);
		un_tipus.add(unitat);

		ButtonGroup ing_tipus = new ButtonGroup();
		ing_tipus.add(ingredient);
		ing_tipus.add(vendible);

		// Grupo de radio

		JLabel idProducte = new JLabel("idProducte");
		idProducte.setBounds(23, 68, 92, 14);
		frame.getContentPane().add(idProducte);

		JLabel nomProducte = new JLabel("nom:");
		nomProducte.setBounds(23, 105, 37, 14);
		frame.getContentPane().add(nomProducte);

		nomProducteField = new JTextField();
		nomProducteField.setBounds(148, 102, 168, 20);
		frame.getContentPane().add(nomProducteField);
		nomProducteField.setColumns(10);

		JLabel lblNewLabel = new JLabel("Preu venda:");
		lblNewLabel.setBounds(23, 161, 79, 14);
		frame.getContentPane().add(lblNewLabel);

		JLabel lblNewJgoodiesLabel_2 = new JLabel("Stock:");
		lblNewJgoodiesLabel_2.setBounds(23, 205, 79, 14);
		frame.getContentPane().add(lblNewJgoodiesLabel_2);

		JLabel lblStockMinim = new JLabel("Stock minim:");
		lblStockMinim.setBounds(23, 253, 77, 14);
		frame.getContentPane().add(lblStockMinim);

		// JEditText Integers

		preuVendaField = new JTextField();
		preuVendaField.setBounds(126, 158, 44, 20);
		((AbstractDocument) preuVendaField.getDocument()).setDocumentFilter(new MyDocumentFilter());
		frame.getContentPane().add(preuVendaField);
		preuVendaField.setColumns(10);

		stockField = new JTextField();
		stockField.setBounds(126, 202, 44, 20);
		((AbstractDocument) stockField.getDocument()).setDocumentFilter(new MyDocumentFilter());
		frame.getContentPane().add(stockField);
		stockField.setColumns(10);

		minStockField = new JTextField();
		minStockField.setBounds(126, 250, 44, 20);
		((AbstractDocument) minStockField.getDocument()).setDocumentFilter(new MyDocumentFilter());
		frame.getContentPane().add(minStockField);
		minStockField.setColumns(10);

		// JEditText Integers

		JLabel lblIdproveidor = new JLabel("idProveidor:");
		lblIdproveidor.setBounds(224, 185, 92, 14);
		frame.getContentPane().add(lblIdproveidor);

		JButton btnModificar = new JButton("Modificar");
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				boolean end = true;
				p.setNomProducte(nomProducteField.getText());
				p.setPreuVenda(Integer.parseInt(preuVendaField.getText()));
				p.setStock(Integer.parseInt(stockField.getText()));
				p.setStockMinim(Integer.parseInt(minStockField.getText()));
				p.setTipus(Tipus.valueOf(getSelectedButton(ing_tipus)));
				p.setUnitatMesura(UnitatMesura.valueOf(getSelectedButton(un_tipus)));
				for (Proveidor pr : mgz.getProveidors()) {
					if (pr.getIdProveidor() == Integer.parseInt(proveidorField.getText())) {
						p.setProveidor(pr);
						end = false;
						break;
					} else {
						end = true;
					}
				}

				end = false;
				if (end == true) {
					JOptionPane.showMessageDialog(frame, "No s'ha trobat el proveidor amb ID corresponent");
				} else {
					JOptionPane.showMessageDialog(frame, "S'ha modificat el producte amb ID: " + p.getCodiProducte());
					frame.dispose();
				}
			}
		});
		btnModificar.setBounds(721, 563, 89, 23);
		frame.getContentPane().add(btnModificar);

		JButton btnAfeguir = new JButton("Afeguir");

		btnAfeguir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (!idProducteField.getText().isEmpty() && !nomProducteField.getText().isEmpty()
						&& !preuVendaField.getText().isEmpty() && !stockField.getText().isEmpty()
						&& !minStockField.getText().isEmpty()) {
					String idProd = idProducteField.getText();
					if (noRepe(idProd, mgz)) {
						Tipus t = Tipus.valueOf(getSelectedButton(ing_tipus));
						UnitatMesura um = UnitatMesura.valueOf(getSelectedButton(un_tipus));
						String nomProd = nomProducteField.getText();
						int preuProd = Integer.parseInt(preuVendaField.getText());
						int stockProd = Integer.parseInt(stockField.getText());
						int minStockProd = Integer.parseInt(minStockField.getText());

						p.setNomProducte(nomProd);
						p.setPreuVenda(preuProd);
						p.setStock(stockProd);
						p.setStockMinim(minStockProd);
						p.setTipus(t);
						p.setUnitatMesura(um);

						a�adirRows(p, dtm2);
						mgz.add(p);

					} else {
						JOptionPane.showMessageDialog(frame, "ID REPETIDA");
					}
				} else {
					JOptionPane.showMessageDialog(frame, "MISSING COMPONENTS");
				}

			}
		});
		btnAfeguir.setBounds(622, 563, 89, 23);
		frame.getContentPane().add(btnAfeguir);

		JButton windowProveidors = new JButton("proveidor");
		windowProveidors.setForeground(Color.BLACK);
		windowProveidors.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Window2 listprov = new Window2(proveidorField, mgz);
				listprov.newScreen(proveidorField, mgz);
			}
		});
		windowProveidors.setBounds(224, 236, 92, 23);
		frame.getContentPane().add(windowProveidors);

		proveidorField = new JTextField();
		proveidorField.setBounds(224, 205, 92, 20);
		proveidorField.setEditable(false);
		if (p.getProveidor() != null) {
			proveidorField.setText(p.getProveidor().getNomProveidor());
		}
		frame.getContentPane().add(proveidorField);
		proveidorField.setColumns(10);

		JLabel lblNewJgoodiesLabel_1 = new JLabel("Composici\u00F3:");
		lblNewJgoodiesLabel_1.setBounds(424, 68, 92, 14);
		frame.getContentPane().add(lblNewJgoodiesLabel_1);

		JLabel lblLots = new JLabel("Lots:");
		lblLots.setBounds(424, 317, 92, 14);
		frame.getContentPane().add(lblLots);

		tableLots = new JTable();
		DefaultTableModel dtmlots = new DefaultTableModel(new Object[][] {},
				new String[] { "Quantitat", "data entrada", "data caducitat" });
		tableLots.setModel(dtmlots);
		tableLots.setBounds(424, 353, 448, 161);
		frame.getContentPane().add(tableLots);

		JButton borrarComp = new JButton("borrar comp");
		borrarComp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (tableComposicio.getSelectedRow() != -1) {
					Object composicio = dtm.getDataVector().elementAt(tableComposicio.getSelectedRow());
					String id = convertidorId(composicio);
					Producte pt = seleccionadorProd(id, mgz);
					p.getComposicio().remove(pt);
					JOptionPane.showMessageDialog(frame, "S'ha borrat el component seleccionat.");
					dtm.removeRow(tableComposicio.getSelectedRow());
				}

			}
		});
		borrarComp.setEnabled(false);
		borrarComp.setBounds(783, 274, 89, 23);
		frame.getContentPane().add(borrarComp);

		JButton afeguirComp = new JButton("afeguir comp");
		afeguirComp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Window3.components(mgz, p, dtm);
			}
		});
		afeguirComp.setBounds(684, 274, 89, 23);
		frame.getContentPane().add(afeguirComp);

		tableComposicio = new JTable();
		tableComposicio.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (tableComposicio.getSelectedRow() != -1) {
					while (dtmlots.getRowCount() > 0)
						dtmlots.removeRow(0);
					borrarComp.setEnabled(true);
					afeguirComp.setEnabled(true);
					if (vendible.isSelected()) {
						Object producto = tableComposicio.getValueAt(tableComposicio.getSelectedRow(), 0);
						Producte p = sacarProd(producto, mgz);
						Object[] lot = new Object[4];
						for (LotDesglossat ld : p.getLots()) {
							lot[0] = ld.getLot();
							lot[1] = ld.getDataEntrada();
							lot[2] = ld.getDataCaducitat();
							lot[3] = ld.getQuantitat();
							dtmlots.addRow(lot);
						}
					}
				}
			}
		});

		tableComposicio.setModel(dtm);
		tableComposicio.setBounds(424, 102, 449, 161);
		frame.getContentPane().add(tableComposicio);

		idProducteField = new JTextField();
		((AbstractDocument) idProducteField.getDocument()).setDocumentFilter(new MyDocumentFilter());
		idProducteField.setBounds(148, 68, 168, 20);
		frame.getContentPane().add(idProducteField);
		idProducteField.setColumns(10);

		// Bordes

		Border border = BorderFactory.createLineBorder(Color.BLACK);
		tableLots
				.setBorder(BorderFactory.createCompoundBorder(border, BorderFactory.createEmptyBorder(10, 10, 10, 10)));

		tableComposicio
				.setBorder(BorderFactory.createCompoundBorder(border, BorderFactory.createEmptyBorder(10, 10, 10, 10)));
		Object[] mostrarComp2 = { "codi producte", "nom producte" };
		DefaultTableModel mostrarComp = new DefaultTableModel(new Object[][] {},
				new String[] { "codi producte", "nom producte" });
		mostrarComp.addRow(mostrarComp2);
		Object[] mostrarLots2 = { "Quantitat", "data entrada", "data caducitat" };
		DefaultTableModel mostrarLots = new DefaultTableModel(new Object[][] {},
				new String[] { "Quantitat", "data entrada", "data caducitat" });
		mostrarLots.addRow(mostrarLots2);

		table = new JTable();
		table.setModel(mostrarLots);
		table.setEnabled(false);
		table.setBounds(424, 331, 448, 23);
		frame.getContentPane().add(table);

		table_1 = new JTable();
		table_1.setModel(mostrarComp);
		table_1.setEnabled(false);
		table_1.setBounds(424, 84, 448, 20);
		frame.getContentPane().add(table_1);

		// Bordes

		// End buttons

		JLabel lblNewJgoodiesTitle = new JLabel(" Producte");
		lblNewJgoodiesTitle.setFont(new Font("Tahoma", Font.ITALIC, 24));
		lblNewJgoodiesTitle.setBackground(new Color(176, 224, 230));

		if (!p.getNomProducte().equals("Juan")) {
			idProducteField.setText(String.valueOf(p.getCodiProducte()));
			nomProducteField.setText(p.getNomProducte());
			preuVendaField.setText(String.valueOf(p.getPreuVenda()));
			stockField.setText(String.valueOf(p.getStock()));
			minStockField.setText(String.valueOf(p.getStockMinim()));
			idProducteField.setEnabled(false);

			while (dtm.getRowCount() > 0)
				dtm.removeRow(0);
			for (Map.Entry pair : p.getComposicio().entrySet()) {
				Producte pt = (Producte) pair.getKey();
				Object[] composicio = new Object[2];
				composicio[0] = pt.getCodiProducte();
				composicio[1] = pt.getNomProducte();
				dtm.addRow(composicio);
			}

			btnAfeguir.setVisible(false);

			if (p.getTipus().equals(Tipus.INGREDIENT)) {
				ingredient.setSelected(true);
			} else {
				vendible.setSelected(true);
			}

			if (p.getUnitatMesura().equals(UnitatMesura.GRAMS)) {
				gram.setSelected(true);
			} else if (p.getUnitatMesura().equals(UnitatMesura.LLITRE)) {
				litre.setSelected(true);
			} else {
				unitat.setSelected(true);
			}
		} else {
			btnModificar.setVisible(false);
		}

		if (ingredient.isSelected()) {
			afeguirComp.setEnabled(false);
		}
	}

	private void a�adirRows(Producte p, DefaultTableModel dtm2) {
		Object[] parts = new Object[6];
		parts[0] = p.getCodiProducte();
		parts[1] = p.getNomProducte();
		parts[2] = p.getStock();
		parts[3] = p.getStockMinim();
		parts[4] = p.getPreuVenda();
		dtm2.addRow(parts);
	}

	private boolean noRepe(String id, Magatzem mgz) {
		for (Producte p : mgz.getProductes()) {
			if (p.getCodiProducte() == Integer.parseInt(id)) {
				return false;
			}
		}

		return true;
	}

	public String getSelectedButton(ButtonGroup ing_tipus) {
		for (Enumeration<AbstractButton> buttons = ing_tipus.getElements(); buttons.hasMoreElements();) {
			AbstractButton button = buttons.nextElement();
			if (button.isSelected()) {
				if (button.getText().equals("GRAM")) {
					return button.getText() + "S";
				}
				return button.getText();
			}
		}
		return "";
	}

	private String convertidorId(Object id) {
		String[] prod = id.toString().split(",");
		String idToString = prod[0];
		idToString = idToString.replaceAll("\\[", "");
		return idToString;
	}

	private Producte seleccionadorProd(String id, Magatzem mgz) {
		Producte p = new Producte();
		for (Producte s : mgz.getProductes()) {
			if (s.getCodiProducte() == Integer.parseInt(id)) {
				p = s;
			}
		}
		return p;
	}

	// sacar prod
	private Producte sacarProd(Object o, Magatzem mgz) {
		String id = convertidorId(o);
		Producte pt = new Producte();
		for (Producte p : mgz.getProductes()) {
			if (p.getCodiProducte() == Integer.parseInt(id)) {
				pt = p;
			}
		}
		return pt;
	}

	class MyDocumentFilter extends DocumentFilter {
		@Override
		public void insertString(FilterBypass fb, int off, String str, AttributeSet attr) throws BadLocationException {
			// remove non-digits
			fb.insertString(off, str.replaceAll("\\D++", ""), attr);
		}

		@Override
		public void replace(FilterBypass fb, int off, int len, String str, AttributeSet attr)
				throws BadLocationException {
			// remove non-digits
			fb.replace(off, len, str.replaceAll("\\D++", ""), attr);
		}
	}
}
