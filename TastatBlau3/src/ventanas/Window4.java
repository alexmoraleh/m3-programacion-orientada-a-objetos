package ventanas;

import tastat.*;
import java.awt.AWTException;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Robot;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;
import java.awt.Font;

public class Window4 {

	private JFrame frame;
	private JTable talbaProductos;
	private JTextField nomProducteField;
	private JTextField idProveidorField;
	private Tipus[] t = new Tipus[2];
	private static String actual;
	private static boolean stupido;
	private static boolean type;
	private static Magatzem mgz1;
	static Producte tet;
	DefaultTableModel dtm = new DefaultTableModel(new Object[][] {},
			new String[] { "codi producte", "nom producte", "stock", "stockMinim", "preu", "Proveidor" });
	private JTextField idProdField;
	private JTextField nomProdField;
	private JTextField stockField;
	private JTextField tipusField;
	private JButton btnBorrar;
	private JButton btnModificar;
	private JButton btnCaducat;
	public static int[][] lots;

	/**
	 * Launch the application.
	 */
	public static void main(Magatzem mgz) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					mgz1 = mgz;
					actual = "";
					type = false;
					stupido = true;

					Window4 window = new Window4(mgz);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Window4(Magatzem mgz) {
		initialize(mgz);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(Magatzem mgz) {
		mgz1 = mgz;
		frame = new JFrame();
		frame.setTitle("Busc_prod");
		frame.getContentPane().setBackground(SystemColor.activeCaption);
		frame.setBackground(SystemColor.textHighlight);
		frame.setBounds(100, 100, 1027, 504);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblNewLabel = new JLabel("    Buscador de Productes");
		lblNewLabel.setFont(new Font("Franklin Gothic Medium", Font.PLAIN, 20));
		lblNewLabel.setOpaque(true);
		lblNewLabel.setAutoscrolls(true);
		lblNewLabel.setForeground(Color.BLACK);
		lblNewLabel.setBackground(SystemColor.windowBorder);
		lblNewLabel.setBounds(0, 0, 1011, 51);
		frame.getContentPane().add(lblNewLabel);

		JLabel lblNomProducte = new JLabel("Nom producte o ID:");
		lblNomProducte.setBounds(65, 85, 131, 14);
		frame.getContentPane().add(lblNomProducte);

		nomProducteField = new JTextField();
		nomProducteField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				robotejo();

				if (nomProducteField.getText().isEmpty()) {
					idProveidorField.setEditable(true);
				} else {
					idProveidorField.setEditable(false);

				}

				actual = nomProducteField.getText();
				actual = actual.trim();
				List<Producte> productes = mgz1.getProductes();
				filtrarProductes(actual, productes, true);
			}
		});
		nomProducteField.setBounds(180, 82, 86, 20);
		frame.getContentPane().add(nomProducteField);
		nomProducteField.setColumns(10);

		JButton btnAadirProducto = new JButton("A\u00F1adir producto");
		btnAadirProducto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Window1.main(mgz1, dtm);
			}
		});
		btnAadirProducto.setBounds(485, 395, 131, 23);
		frame.getContentPane().add(btnAadirProducto);

		btnModificar = new JButton("MODIFICAR");
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Producte p = sacarProd(talbaProductos.getValueAt(talbaProductos.getSelectedRow(), 0));
				Window1.main(p, mgz1, dtm);
			}
		});
		btnModificar.setBounds(864, 395, 100, 23);
		btnModificar.setEnabled(false);
		frame.getContentPane().add(btnModificar);

		btnBorrar = new JButton("BORRAR");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (talbaProductos.getSelectedRow() != -1) {
					Object producto = talbaProductos.getValueAt(talbaProductos.getSelectedRow(), 0);
					String id = convertidorId(producto);
					for (int i = 0; i < mgz1.getProductes().size(); i++) {
						Producte p = mgz1.getProductes().get(i);
						if (p.getCodiProducte() == Integer.parseInt(id)) {
							mgz1.getProductes().remove(i);
							JOptionPane.showMessageDialog(frame, "S'ha borrat amb exit el producte amb ID: " + id);
							dtm.removeRow(talbaProductos.getSelectedRow());
							refreshObjects();
						}
					}
				}
			}
		});
		btnBorrar.setEnabled(false);
		btnBorrar.setBounds(746, 395, 100, 23);
		frame.getContentPane().add(btnBorrar);

		talbaProductos = new JTable();
		talbaProductos.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (talbaProductos.getSelectedRow() != -1) {
					btnBorrar.setEnabled(true);
					btnModificar.setEnabled(true);

					Object producto = talbaProductos.getValueAt(talbaProductos.getSelectedRow(), 0);
					String id = convertidorId(producto);
					for (int i = 0; i < mgz1.getProductes().size(); i++) {
						Producte p = mgz1.getProductes().get(i);
						if (p.getCodiProducte() == Integer.parseInt(id)) {
							idProdField.setText(String.valueOf(p.getCodiProducte()));
							nomProdField.setText(p.getNomProducte());
							stockField.setText(String.valueOf(p.getStock()));
							tipusField.setText(String.valueOf(p.getTipus()));
							Date avui = new Date();
							boolean caducat = false;
							lots = new int[p.getLots().size()][2];
							int x = 0;
							caducat = Lotes(p.getLots(), caducat);
							btnCaducat.setEnabled(true);

							if (lots.length > 0) {
							} else {
								for (Map.Entry pair : p.getComposicio().entrySet()) {
									Producte pt = (Producte) pair.getKey();
									lots = new int[pt.getLots().size()][2];
									caducat = Lotes(pt.getLots(), caducat);
								}
							}

							caducadosONo(caducat);
						}
					}
				}
			}
		});
		talbaProductos.setBounds(30, 162, 584, 205);
		talbaProductos.setModel(dtm);
		frame.getContentPane().add(talbaProductos);

		JLabel lblProducteActual = new JLabel("Producte actual");
		lblProducteActual.setBounds(787, 57, 90, 14);
		frame.getContentPane().add(lblProducteActual);

		JLabel lblMagatzem = new JLabel("Magatzem");
		lblMagatzem.setBounds(41, 162, 66, 14);
		frame.getContentPane().add(lblMagatzem);

		JLabel lblNewLabel_1 = new JLabel("Tipus producte");
		lblNewLabel_1.setBounds(485, 54, 90, 20);
		frame.getContentPane().add(lblNewLabel_1);

		// Radio buttons

		ButtonGroup un_tipus = new ButtonGroup();

		JRadioButton rdbtnNewRadioButton = new JRadioButton("Tots");
		rdbtnNewRadioButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				t[0] = Tipus.INGREDIENT;
				t[1] = Tipus.VENDIBLE;
				type = true;
				filtrarProductesPerTipus();
			}
		});
		rdbtnNewRadioButton.setBounds(474, 81, 109, 23);
		frame.getContentPane().add(rdbtnNewRadioButton);

		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Vendible");
		rdbtnNewRadioButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				t[0] = Tipus.valueOf(selectedButton(un_tipus));
				t[1] = Tipus.NULL;
				type = true;
				filtrarProductesPerTipus();
			}
		});
		rdbtnNewRadioButton_1.setBounds(474, 106, 109, 23);
		frame.getContentPane().add(rdbtnNewRadioButton_1);

		JRadioButton rdbtnNewRadioButton_2 = new JRadioButton("Ingredient");
		rdbtnNewRadioButton_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				t[0] = Tipus.valueOf(selectedButton(un_tipus));
				t[1] = Tipus.NULL;
				type = true;
				filtrarProductesPerTipus();
			}
		});
		rdbtnNewRadioButton_2.setBounds(474, 132, 109, 23);
		frame.getContentPane().add(rdbtnNewRadioButton_2);

		// Radio buttons end

		JLabel lblIdproveidor = new JLabel("idProveidor:");
		lblIdproveidor.setBounds(94, 114, 76, 14);
		frame.getContentPane().add(lblIdproveidor);

		idProveidorField = new JTextField();
		idProveidorField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				robotejo();

				String prov = idProveidorField.getText();
				buscarProv(prov);
			}
		});
		idProveidorField.setBounds(180, 111, 86, 20);
		frame.getContentPane().add(idProveidorField);
		idProveidorField.setColumns(10);

		JLabel lblBuscarPer = new JLabel("Buscar per");
		lblBuscarPer.setBounds(119, 54, 77, 20);
		frame.getContentPane().add(lblBuscarPer);

		Border border = BorderFactory.createLineBorder(Color.BLACK);

		un_tipus.add(rdbtnNewRadioButton_2);
		un_tipus.add(rdbtnNewRadioButton_1);
		un_tipus.add(rdbtnNewRadioButton);
		talbaProductos.setBorder(border);

		idProdField = new JTextField();
		idProdField.setEditable(false);
		idProdField.setBounds(714, 126, 86, 20);
		frame.getContentPane().add(idProdField);
		idProdField.setColumns(10);

		JLabel lblIdprod = new JLabel("idProd");
		lblIdprod.setBounds(723, 103, 46, 14);
		frame.getContentPane().add(lblIdprod);

		JLabel lblNewLabel_2 = new JLabel("nomProd");
		lblNewLabel_2.setBounds(724, 162, 46, 14);
		frame.getContentPane().add(lblNewLabel_2);

		nomProdField = new JTextField();
		nomProdField.setEditable(false);
		nomProdField.setBounds(714, 186, 86, 20);
		frame.getContentPane().add(nomProdField);
		nomProdField.setColumns(10);

		JLabel lblStock = new JLabel("stock");
		lblStock.setBounds(723, 228, 46, 14);
		frame.getContentPane().add(lblStock);

		stockField = new JTextField();
		stockField.setEditable(false);
		stockField.setBounds(714, 245, 86, 20);
		frame.getContentPane().add(stockField);
		stockField.setColumns(10);

		tipusField = new JTextField();
		tipusField.setEditable(false);
		tipusField.setBounds(853, 162, 86, 20);
		frame.getContentPane().add(tipusField);
		tipusField.setColumns(10);

		rdbtnNewRadioButton.setSelected(true);

		btnCaducat = new JButton("");
		btnCaducat.setEnabled(false);
		btnCaducat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Window5.main(lots);
			}
		});
		btnCaducat.setBounds(839, 125, 100, 23);
		frame.getContentPane().add(btnCaducat);

		for (Producte p : mgz1.getProductes()) {
			Object[] parts = new Object[5];
			parts[0] = p.getCodiProducte();
			parts[1] = p.getNomProducte();
			parts[2] = p.getStock();
			parts[3] = p.getStockMinim();
			parts[4] = p.getPreuVenda();
			dtm.addRow(parts);
		}
	}

	// Buscar prov

	private void buscarProv(String prov) {
		refreshObjects();
		for (Producte p : mgz1.getProductes()) {
			if (p.getProveidor() != null) {
				if (searchAll(prov.toLowerCase(), p.getProveidor().getNomProveidor().toLowerCase())) {
					a�adirRows(p);
				}
			}
		}
	}

	// Producte lot

	private boolean Lotes(List<LotDesglossat> ld, boolean caducat) {
		Date avui = new Date();
		int x = 0;
		for (LotDesglossat ls : ld) {
			if (avui.compareTo(ls.getDataCaducitat()) < 0) {
				lots[x][0] = ls.getLot();
				lots[x][1] = 1;
				caducat = true;
			} else {
				lots[x][0] = ls.getLot();
				lots[x][1] = 0;
			}
			x++;
		}

		return caducat;
	}

	// Asignar el contingut corresponent al btn

	public void caducadosONo(Boolean caducat) {
		if (caducat) {
			btnCaducat.setText("CADUCADO");
			btnCaducat.setForeground(Color.RED);
		} else if (!caducat) {
			if (lots.length > 0) {
				btnCaducat.setText("VIGENTE");
				btnCaducat.setForeground(Color.GREEN);
			} else {
				btnCaducat.setText("�SIN LOTE!");
				btnCaducat.setForeground(Color.YELLOW);
			}
		}
	}

	// Refresh list and butt

	private void refreshObjects() {
		while (dtm.getRowCount() > 0)
			dtm.removeRow(0);
		btnCaducat.setEnabled(false);
		btnBorrar.setEnabled(false);
		btnModificar.setEnabled(false);
		idProdField.setText("");
		nomProdField.setText("");
		stockField.setText("");
		tipusField.setText("");
		btnCaducat.setText("");
	}

	// SACAR PRODUCTO
	private Producte sacarProd(Object o) {
		String id = convertidorId(o);
		Producte pt = new Producte();
		for (Producte p : mgz1.getProductes()) {
			if (p.getCodiProducte() == Integer.parseInt(id)) {
				pt = p;
			}
		}
		return pt;
	}

	// Robot PULSAR boton

	private void robotejo() {
		Robot r;
		try {
			if (stupido) {
				stupido = false;
				r = new Robot();
				r.keyPress(KeyEvent.VK_ENTER);
				r.keyRelease(KeyEvent.VK_ENTER);
			} else {
				stupido = true;
			}
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Buscar producto

	private void filtrarProductes(String id, List<Producte> p, boolean string) {
		refreshObjects();
		while (dtm.getRowCount() > 0)
			dtm.removeRow(0);
		for (int i = 0; i < p.size(); i++) {
			if (!id.isEmpty()) {
				if (string && searchAll(id.toLowerCase(), p.get(i).getNomProducte().toLowerCase()) && !id.equals("0")) {
					if (type) {
						if (filtrarTipo(p.get(i).getTipus())) {
							a�adirRows(p.get(i));
						}
					} else {
						a�adirRows(p.get(i));
					}
				} else if (!id.equals("0")) {
					if (searchAll(id.toLowerCase(), String.valueOf(p.get(i).getCodiProducte()))) {
						if (type) {
							if (filtrarTipo(p.get(i).getTipus())) {
								a�adirRows(p.get(i));
							}
						} else {
							if (type) {
								if (filtrarTipo(p.get(i).getTipus())) {
									a�adirRows(p.get(i));
								}
							} else {
								a�adirRows(p.get(i));
							}

						}
					}
				} else {
					a�adirRows(p.get(i));
				}
			} else {
				while (dtm.getRowCount() > 0)
					dtm.removeRow(0);
				for (Producte pt : mgz1.getProductes()) {
					Object[] parts = new Object[5];
					parts[0] = pt.getCodiProducte();
					parts[1] = pt.getNomProducte();
					parts[2] = pt.getStock();
					parts[3] = pt.getStockMinim();
					parts[4] = pt.getPreuVenda();
					dtm.addRow(parts);
				}
			}
		}
	}

	// A�adir cosas a tabla productos

	private void a�adirRows(Producte p) {
		Object[] parts = new Object[6];
		parts[0] = p.getCodiProducte();
		parts[1] = p.getNomProducte();
		parts[2] = p.getStock();
		parts[3] = p.getStockMinim();
		parts[4] = p.getPreuVenda();
		dtm.addRow(parts);
	}

	// Filtrar en caso de que hayas seleccionado el botton despues de hacer la
	// consulta

	private void filtrarProductesPerTipus() {

		List<Producte> productes = mgz1.getProductes();

		while (dtm.getRowCount() > 0)
			dtm.removeRow(0);

		if (!nomProducteField.getText().isEmpty() || !idProveidorField.getText().isEmpty()) {
			for (int i = 0; i < productes.size(); i++) {
				if (!actual.isEmpty()) {
					if (searchAll(actual.toLowerCase(), productes.get(i).getNomProducte().toLowerCase())) {
						if (filtrarTipo(productes.get(i).getTipus())) {
							Object[] parts = new Object[5];
							parts[0] = productes.get(i).getCodiProducte();
							parts[1] = productes.get(i).getNomProducte();
							parts[2] = productes.get(i).getStock();
							parts[3] = productes.get(i).getStockMinim();
							parts[4] = productes.get(i).getPreuVenda();

							dtm.addRow(parts);
						}
					}
				}
			}
		}
	}

	// Quitar la ID del objeto

	private String convertidorId(Object id) {
		String[] prod = id.toString().split(",");
		String idToString = prod[0];
		idToString = idToString.replaceAll("\\[", "");
		return idToString;
	}

	// Filtro rapido para el tipo

	private boolean filtrarTipo(Tipus a) {
		for (int x = 0; x < t.length; x++) {
			if (a == t[x]) {
				return true;
			}
		}
		return false;
	}

	// Pillar el botton seleccioando dentro de un grupo

	private String selectedButton(ButtonGroup id) {
		for (Enumeration<AbstractButton> buttons = id.getElements(); buttons.hasMoreElements();) {
			AbstractButton button = buttons.nextElement();

			if (button.isSelected()) {
				return button.getText().toUpperCase();
			}
		}

		return null;
	}

	// Buscar para todos

	private boolean searchAll(String id, String cod) {
		if (id.length() < cod.length()) {
			while (id.length() < cod.length()) {
				cod = cortarString(cod);
			}
			if (id.equals(cod)) {
				return true;
			} else {
				return false;
			}
		} else if (id.equals(cod)) {
			return true;
		} else {
			return false;
		}
	}

	// Submetodo para quitar el ultimo caracter de un String

	private String cortarString(String str) {
		if (str != null && str.length() > 0) {
			str = str.substring(0, str.length() - 1);
		}
		return str;
	}

	// Filtro para quitar los no-numeros en un campo

	class MyDocumentFilter extends DocumentFilter {
		@Override
		public void insertString(FilterBypass fb, int off, String str, AttributeSet attr) throws BadLocationException {
			// remove non-digits
			fb.insertString(off, str.replaceAll("\\D++", ""), attr);
		}

		@Override
		public void replace(FilterBypass fb, int off, int len, String str, AttributeSet attr)
				throws BadLocationException {
			// remove non-digits
			fb.replace(off, len, str.replaceAll("\\D++", ""), attr);
		}
	}
}
