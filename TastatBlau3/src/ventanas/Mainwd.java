package ventanas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.BorderLayout;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.FormSpecs;
import java.awt.CardLayout;
import javax.swing.SpringLayout;
import java.awt.GridLayout;
import net.miginfocom.swing.MigLayout;
import tastat.Magatzem;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import java.awt.Color;

public class Mainwd {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Mainwd window = new Mainwd();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the application.
	 */
	public Mainwd(Magatzem m) {
		initialize(m);
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(Magatzem m) {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.DARK_GRAY);
		frame.setBounds(100, 100, 312, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		JLabel lblAlmacen = new JLabel("Almac\u00E9n");
		lblAlmacen.setForeground(Color.WHITE);
		lblAlmacen.setHorizontalAlignment(SwingConstants.CENTER);
		lblAlmacen.setFont(new Font("Tahoma", Font.PLAIN, 21));
		
		JButton btnClientes = new JButton("Clientes");
		btnClientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Clientwd window = new ventanas.Clientwd(m);
			}
		});
		frame.getContentPane().setLayout(new GridLayout(0, 1, 0, 0));
		frame.getContentPane().add(lblAlmacen);
		frame.getContentPane().add(btnClientes);
		
		JButton btnComand = new JButton("Comandas");
		btnComand.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Interficie2 window2 = new ventanas.Interficie2(m);
				window2.setVisible(true);
			}
		});
		frame.getContentPane().add(btnComand);
		
		JButton btnProd = new JButton("Productos");
		btnProd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Mierda de Oriol
				Window4 window3 = new Window4(m);
				window3.main(m);
			}
		});
		frame.getContentPane().add(btnProd);
	}
}
