package ventanas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;

import tastat.Client;
import tastat.Comanda;
import tastat.ComandaEstat;
import tastat.Magatzem;

import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Date;
import java.awt.event.ActionEvent;
import java.awt.List;
import javax.swing.JList;
import java.awt.Choice;
import javax.swing.DefaultComboBoxModel;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.Toolkit;

public class Interficie {

	private JFrame frame;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(Magatzem elMeuMagatzem) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interficie window = new Interficie(elMeuMagatzem);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interficie(Magatzem elMeuMagatzem) {
		initialize( elMeuMagatzem);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(Magatzem elMeuMagatzem) {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(0, 255, 255));
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage("E:\\Favicon.ico.ico"));
		frame.getContentPane().setForeground(new Color(0, 255, 255));
		frame.setFont(new Font("Bahnschrift", Font.PLAIN, 12));
		frame.setTitle(" ");
		frame.setBounds(100, 100, 264, 361);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblIdcomanda = new JLabel("IdComandas");
		lblIdcomanda.setBounds(10, 11, 83, 14);
		frame.getContentPane().add(lblIdcomanda);
		
		textField = new JTextField();
		textField.setBounds(111, 8, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblNombreEmpresa = new JLabel("Nombre Empresa");
		lblNombreEmpresa.setBounds(10, 36, 89, 14);
		frame.getContentPane().add(lblNombreEmpresa);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(0, 255, 255));
		panel.setBounds(10, 108, 224, 119);
		frame.getContentPane().add(panel);
		
		JRadioButton radioButton = new JRadioButton("Pendiente");
		radioButton.setBackground(new Color(0, 255, 255));
		panel.add(radioButton);
		
		JRadioButton radioButton_1 = new JRadioButton("Preparada");
		radioButton_1.setBackground(new Color(0, 255, 255));
		panel.add(radioButton_1);
		
		JRadioButton radioButton_2 = new JRadioButton("Transport");
		radioButton_2.setBackground(new Color(0, 255, 255));
		panel.add(radioButton_2);
		
		JRadioButton radioButton_3 = new JRadioButton("Lliurada");
		radioButton_3.setBackground(new Color(0, 255, 255));
		panel.add(radioButton_3);
		ButtonGroup grupo1 = new ButtonGroup();
		grupo1.add(radioButton);
		grupo1.add(radioButton_1);
		grupo1.add(radioButton_3);
		grupo1.add(radioButton_2);
		JLabel lblEstat = new JLabel("Estat");
		lblEstat.setBounds(39, 81, 66, 14);
		frame.getContentPane().add(lblEstat);
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"..."}));
		comboBox.setBounds(111, 39, 86, 20);
		frame.getContentPane().add(comboBox);
		for(Client d:  elMeuMagatzem.getClients() ) {
			comboBox.addItem(d.getNomClient());
		}
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Comanda c = new Comanda();
	Date fecha = new Date();
				c.setDataComanda(fecha);
				c.setDataLliurament(fecha);
				c.setIdComanda(Integer.parseInt(textField.getText()));
				
				if(radioButton_3.isSelected()) {//* is= es
					c.setEstat(ComandaEstat.LLIURADA);
				}
				else if(radioButton.isSelected()) {//* is= es
					c.setEstat(ComandaEstat.PENDENT);
				}
				else if(radioButton_1.isSelected()) {//* is= es
					c.setEstat(ComandaEstat.PREPARADA);
				}
				else if(radioButton_2.isSelected()) {//* is= es
					c.setEstat(ComandaEstat.TRANSPORT);
				}
				Client client = new Client();
				for(Client d:  elMeuMagatzem.getClients()) {
					if(d.getNomClient()==comboBox.getSelectedItem()) {
						client=d;
					}
					
				}
				c.setClient(client);
				 elMeuMagatzem.getComandes().add(c);
			}
		});
		btnGuardar.setBounds(81, 288, 89, 23);
		frame.getContentPane().add(btnGuardar);
		
		
	}
}
