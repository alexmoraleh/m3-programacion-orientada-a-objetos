package ventanas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import java.awt.Font;
import java.awt.Frame;

import javax.swing.JTextField;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import tastat.Client;
import tastat.Magatzem;

import javax.swing.border.BevelBorder;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Vector;
import java.awt.event.ActionEvent;
import javax.swing.ListSelectionModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JRadioButton;

public class Clientwd extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JFrame frame;
	private JTextField txtBusqueda;
	private JTable table;

	/**
	 * Launch the application.
	 */
//	private static Magatzem m=tastat.Programa.main(null);
	private JTable table_1;
/*	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Clientwd window = new Clientwd(m);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the application.
	 */
	public Clientwd(Magatzem m) {
		initialize(m);
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private JFrame initialize(Magatzem m) {
		frame = new JFrame();
		frame.getContentPane().setBackground(SystemColor.menu);
		frame.setBounds(100, 100, 831, 510);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setTitle("Clientes");
		
		JTextPane txtpnClients = new JTextPane();
		txtpnClients.setEditable(false);
		txtpnClients.setBackground(SystemColor.menu);
		txtpnClients.setFont(new Font("Arial", Font.PLAIN, 20));
		txtpnClients.setText("Clientes");
		txtpnClients.setBounds(10, 11, 81, 24);
		frame.getContentPane().add(txtpnClients);
		
		txtBusqueda = new JTextField();
		txtBusqueda.setBounds(64, 46, 125, 20);
		frame.getContentPane().add(txtBusqueda);
		txtBusqueda.setColumns(10);
		
		JTextPane txtpnBuscar = new JTextPane();
		txtpnBuscar.setEditable(false);
		txtpnBuscar.setBackground(SystemColor.menu);
		txtpnBuscar.setText("Buscar:");
		txtpnBuscar.setBounds(10, 46, 38, 20);
		frame.getContentPane().add(txtpnBuscar);
		
		
		
		
		table = new JTable();
		table.setSurrendersFocusOnKeystroke(true);
		table.setEnabled(false);
		table.setColumnSelectionAllowed(true);
		table.setCellSelectionEnabled(true);
		table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		table.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		DefaultTableModel dtm = new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
						"ID", "Nombre", "CIF", "Activo", "Direcci\u00F3n", "Poblaci\u00F3n", "Pais", "Contacto", "Telefono", "Latitud", "Longitud"
					}
				){private static final long serialVersionUID = 1L;
				@SuppressWarnings("rawtypes")
				Class[] columnTypes = new Class[] {
						Integer.class, String.class, Object.class, Boolean.class, Object.class, Object.class, Object.class, Object.class, Object.class, Object.class, Object.class
					};
					public Class getColumnClass(int columnIndex) {
						return columnTypes[columnIndex];
					}
				};
		table.setModel(dtm);
		table.setBounds(41, 92, 764, 283);
		frame.getContentPane().add(table);
		
		
		JButton btnCerca = new JButton("Buscar");
		JRadioButton rdbn1 = new JRadioButton("");
		rdbn1.setBounds(14, 92, 21, 15);
		rdbn1.setVisible(false);
		frame.getContentPane().add(rdbn1);
		
		JRadioButton rdbn2 = new JRadioButton("");
		rdbn2.setBounds(14, 107, 21, 15);
		rdbn2.setVisible(false);
		frame.getContentPane().add(rdbn2);
		
		JRadioButton rdbn3 = new JRadioButton("");
		rdbn3.setBounds(14, 123, 21, 15);
		rdbn3.setVisible(false);
		frame.getContentPane().add(rdbn3);
		
		JRadioButton rdbn4 = new JRadioButton("");
		rdbn4.setBounds(14, 140, 21, 15);
		rdbn4.setVisible(false);
		frame.getContentPane().add(rdbn4);
		
		JRadioButton rdbn5 = new JRadioButton("");
		rdbn5.setBounds(14, 156, 21, 15);
		rdbn5.setVisible(false);
		frame.getContentPane().add(rdbn5);
		
		JRadioButton rdbn6 = new JRadioButton("");
		rdbn6.setBounds(14, 172, 21, 15);
		rdbn6.setVisible(false);
		frame.getContentPane().add(rdbn6);
		
		JRadioButton rdbn7 = new JRadioButton("");
		rdbn7.setBounds(14, 188, 21, 15);
		rdbn7.setVisible(false);
		frame.getContentPane().add(rdbn7);
		
		ButtonGroup grouprb = new ButtonGroup();
		grouprb.add(rdbn1);
		grouprb.add(rdbn2);
		grouprb.add(rdbn3);
		grouprb.add(rdbn4);
		grouprb.add(rdbn5);
		grouprb.add(rdbn6);
		grouprb.add(rdbn7);
		ArrayList<JRadioButton>bnlst=new ArrayList<JRadioButton>();
		bnlst.add(rdbn1);
		bnlst.add(rdbn2);
		bnlst.add(rdbn3);
		bnlst.add(rdbn4);
		bnlst.add(rdbn5);
		bnlst.add(rdbn6);
		bnlst.add(rdbn7);
		ArrayList<Client>clt=new ArrayList<Client>();
		ActionListener tabla=new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println(dtm.getRowCount());
				m.getClients().sort(new Comparator<Client>() {

					@Override
					public int compare(Client o1, Client o2) {
						// TODO Auto-generated method stub
						if(o1.getIdClient()-o2.getIdClient()>0) return 1;
						else return -1;
					}
				});
				if(dtm.getRowCount()>0) {
					while(dtm.getRowCount()>0) {
						dtm.removeRow(dtm.getRowCount()-1);
					}
					
				}
				for(int i=0;i<7;i++) {
					bnlst.get(i).setVisible(false);
					grouprb.setSelected(bnlst.get(i).getModel(), true);
				}
				//grouprb.getSelection().setSelected(true);
				clt.clear();
				int x=0;
				for(Client c:m.getClients()) {
					if(c.nomClient.toLowerCase().contains(txtBusqueda.getText().toLowerCase())) {
						clt.add(c);
						dtm.addRow(c.getTodo().toArray());//Acabar Array de Client
						bnlst.get(x).setVisible(true);
						x++;
						//table.setModel(dtm);
					}
				}
			}
		};
		btnCerca.addActionListener(tabla);
		txtBusqueda.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				tabla.actionPerformed(null);
			}
		});
		System.out.println(m.getClients().get(0).nomClient.toString());
		btnCerca.setBounds(213, 45, 81, 23);
		frame.getContentPane().add(btnCerca);
		
		table_1 = new JTable(new DefaultTableModel(
				new Object[][] {
					{"ID", "Nombre", "CIF", "Activo", "Direcci\u00F3n", "Poblaci\u00F3n", "Pais", "Contacto", "Telefono", "Latitud", "Longitud"},
				},
				new String[] {
						"ID", "Nombre", "CIF", "Activo", "Direcci\u00F3n", "Poblaci\u00F3n", "Pais", "Contacto", "Telefono", "Latitud", "Longitud"
					}
				));
		table_1.setEnabled(false);
		table_1.setBackground(SystemColor.window);
		table_1.setBounds(41, 77, 764, 15);
		frame.getContentPane().add(table_1);		
		
		JButton btnDonarDeBaixa = new JButton("Dar de baja");
		btnDonarDeBaixa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean exit=false;
				for(int i=0;i<clt.size() && exit==false;i++) {
					if(bnlst.get(i).isSelected()) {
						clt.get(i).setActiu(false);
						JOptionPane.showMessageDialog(frame, "Succes");
						exit=true;
					} 
				}
				if(!exit) JOptionPane.showMessageDialog(frame, "Seleccione que borrar");
				tabla.actionPerformed(null);
			}
		});
		btnDonarDeBaixa.setBounds(628, 386, 120, 23);
		frame.getContentPane().add(btnDonarDeBaixa);
		
		JButton btnModificar = new JButton("Modificar");
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//JOptionPane.showMessageDialog(frame, "A basic JOptionPane message dialog");
				boolean exit=false;
				for(int i=0;i<clt.size() && exit==false;i++) {
					if(bnlst.get(i).isSelected()) {
						frame.setEnabled(false);
						Managecltwd win =new Managecltwd(m,(int)((Vector)dtm.getDataVector().elementAt(i)).get(0),frame);
						exit=true;
					} 
				}
				if(!exit) {
					JOptionPane.showMessageDialog(frame, "Seleccione que modificar");
				}
				tabla.actionPerformed(null);
			}
		});
		btnModificar.setBounds(362, 386, 89, 23);
		frame.getContentPane().add(btnModificar);
		JButton btnDonarDalta = new JButton("Dar de alta");
		btnDonarDalta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setEnabled(false);
				Managecltwd win=new Managecltwd(m,frame);
			}
		});
		btnDonarDalta.setBounds(99, 386, 105, 23);
		frame.getContentPane().add(btnDonarDalta);
		return frame;
		
		
	}
	
}
