package ventanas;

import tastat.*;
import java.awt.AWTException;
import java.awt.EventQueue;
import java.awt.Robot;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Enumeration;
import java.util.List;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import com.jgoodies.forms.factories.DefaultComponentFactory;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;

public class Window3 {

	private JTextField nomProducteField;
	private JFrame frame;
	private JTable ingredients;
	DefaultTableModel dtm = new DefaultTableModel(new Object[][] {},
			new String[] { "codi producte", "nom producte", "stock", "stockMinim", "preu", "Proveidor" });

	private Tipus[] t = { Tipus.INGREDIENT, Tipus.NULL };
	private static String actual;
	private static boolean stupido;

	/**
	 * Launch the application.
	 */
	public static void components(Magatzem mgz, Producte p, DefaultTableModel dtm2) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window3 window = new Window3(mgz, p, dtm2);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Window3(Magatzem mgz, Producte p, DefaultTableModel dtm2) {
		initialize(mgz, p, dtm2);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(Magatzem mgz, Producte p, DefaultTableModel dtm2) {
		frame = new JFrame();
		frame.setTitle("Prod_Af_Ingred");
		frame.getContentPane().setBackground(SystemColor.activeCaption);
		frame.setBounds(100, 100, 500, 381);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		ingredients = new JTable();
		ingredients.setBounds(10, 61, 464, 238);
		ingredients.setModel(dtm);
		frame.getContentPane().add(ingredients);

		nomProducteField = new JTextField();
		nomProducteField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				robotejo();

				actual = nomProducteField.getText();
				actual = actual.trim();
				filtrarProductesPerTipus(mgz);
			}
		});
		nomProducteField.setBounds(10, 30, 98, 20);
		frame.getContentPane().add(nomProducteField);
		nomProducteField.setColumns(10);

		JLabel lblIngredientsDisponibles = new JLabel("INGREDIENTS DISPONIBLES");
		lblIngredientsDisponibles.setBounds(10, 11, 178, 14);
		frame.getContentPane().add(lblIngredientsDisponibles);

		JButton btnAceptar = new JButton("ACEPTAR");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Object[] o = new Object[2];
				Producte pt = sacarProd(convertidorId(ingredients.getValueAt(ingredients.getSelectedRow(), 0)), mgz);
				p.afegirComponent(pt, 10);
				o[0] = pt.getCodiProducte();
				o[1] = pt.getNomProducte();
				dtm2.addRow(o);
				frame.dispose();
			}
		});
		btnAceptar.setBounds(10, 310, 89, 23);
		frame.getContentPane().add(btnAceptar);

		for (Producte pte : mgz.getProductes()) {
			if (pte.getTipus().equals(Tipus.INGREDIENT)) {
				Object[] parts = new Object[5];
				parts[0] = pte.getCodiProducte();
				parts[1] = pte.getNomProducte();
				parts[2] = pte.getStock();
				parts[3] = pte.getStockMinim();
				parts[4] = pte.getPreuVenda();

				dtm.addRow(parts);
			}
		}
	}

	private void robotejo() {
		Robot r;
		try {
			if (stupido) {
				stupido = false;
				r = new Robot();
				r.keyPress(KeyEvent.VK_ENTER);
				r.keyRelease(KeyEvent.VK_ENTER);
			} else {
				stupido = true;
			}
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Filtrar en caso de que hayas seleccionado el botton despues de hacer la
	// consulta

	private void filtrarProductesPerTipus(Magatzem mgz) {

		List<Producte> productes = mgz.getProductes();

		while (dtm.getRowCount() > 0)
			dtm.removeRow(0);

		if (!nomProducteField.getText().isEmpty()) {
			for (int i = 0; i < productes.size(); i++) {
				if (!actual.isEmpty()) {
					if (searchAll(actual.toLowerCase(), productes.get(i).getNomProducte().toLowerCase())) {
						System.out.println(
								searchAll(actual.toLowerCase(), productes.get(i).getNomProducte().toLowerCase()));
						if (filtrarTipo(productes.get(i).getTipus())) {
							Object[] parts = new Object[5];
							parts[0] = productes.get(i).getCodiProducte();
							parts[1] = productes.get(i).getNomProducte();
							parts[2] = productes.get(i).getStock();
							parts[3] = productes.get(i).getStockMinim();
							parts[4] = productes.get(i).getPreuVenda();

							dtm.addRow(parts);
						}
					}
				}
			}
		} else {
			for (Producte p : mgz.getProductes()) {
				Object[] parts = new Object[5];
				parts[0] = p.getCodiProducte();
				parts[1] = p.getNomProducte();
				parts[2] = p.getStock();
				parts[3] = p.getStockMinim();
				parts[4] = p.getPreuVenda();
				dtm.addRow(parts);
			}
		}
	}

	private String convertidorId(Object id) {
		String[] prod = id.toString().split(",");
		String idToString = prod[0];
		idToString = idToString.replaceAll("\\[", "");
		return idToString;
	}

	private Producte sacarProd(Object o, Magatzem mgz) {
		String id = convertidorId(o);
		Producte pt = new Producte();
		for (Producte p : mgz.getProductes()) {
			if (p.getCodiProducte() == Integer.parseInt(id)) {
				pt = p;
			}
		}
		return pt;
	}

	// Filtro rapido para el tipo

	private boolean filtrarTipo(Tipus a) {
		for (int x = 0; x < t.length; x++) {
			if (a == t[x]) {
				return true;
			}
		}
		return false;
	}

	// Buscar para todos

	private boolean searchAll(String id, String cod) {
		if (id.length() < cod.length()) {
			while (id.length() < cod.length()) {
				cod = cortarString(cod);
			}
			if (id.equals(cod)) {
				return true;
			} else {
				return false;
			}
		} else if (id.equals(cod)) {
			return true;
		} else {
			return false;
		}
	}

	// Submetodo para quitar el ultimo caracter de un String

	private String cortarString(String str) {
		if (str != null && str.length() > 0) {
			str = str.substring(0, str.length() - 1);
		}
		return str;
	}
}
