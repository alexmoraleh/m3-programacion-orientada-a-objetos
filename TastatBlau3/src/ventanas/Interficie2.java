 package ventanas;

import java.awt.BorderLayout;

import tastat.Client;
import tastat.Comanda;
import tastat.ComandaEstat;
import tastat.Magatzem;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.TextField;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.JScrollBar;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import java.awt.Choice;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.awt.event.ActionEvent;
import java.awt.List;
import javax.swing.JRadioButton;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.border.BevelBorder;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JEditorPane;
import javax.swing.JTabbedPane;
import java.awt.Color;
import javax.swing.table.DefaultTableModel;
import java.awt.Button;
import javax.swing.border.CompoundBorder;


public class Interficie2 extends JFrame implements ActionListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DefaultTableModel dtm= new DefaultTableModel(
			new Object[][] {
				{"Importe", "Cantidad", "Nombre Producto", "Precio Venta"},
			},
			new String[] {
				"Importe", "Cantidad",  "Nombre Producto", "Precio Venta"
			}
		);
	private JPanel contentPane;
	private JTextField txt_IdComanda;
	private JLabel lblClient;
	private JTextField txtIdClient;
	private JTextField txtDataComanda;
	private JTextField txtDataEntrega;
	private JPanel panel;
	private JRadioButton Lliurada;
	private JRadioButton Pendiente;
	private JRadioButton Transport;
	private JRadioButton Preparada;
	private JLabel lblEstadi;
	private JTextField txtNomClient;
	private JTable table;
	private JTable table_1;
	private JTextField txtTextoPorDefecto;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
				//	Interficie2 frame = new Interficie2();
				//	frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Interficie2(Magatzem elMeuMagatzem) {

		setTitle("Buscar Comandas");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setForeground(new Color(0, 255, 255));
		contentPane.setBackground(new Color(0, 255, 255));
		setSize(650,700);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblComanda = new JLabel("Comanda");
		lblComanda.setBounds(10, 24, 66, 14);
		contentPane.add(lblComanda);
		
		txt_IdComanda = new JTextField();
		txt_IdComanda.setText("1");
		txt_IdComanda.setBounds(71, 21, 86, 20);
		contentPane.add(txt_IdComanda);
		txt_IdComanda.setColumns(10);
		txt_IdComanda.addActionListener(this);
		lblClient = new JLabel("Client");
		lblClient.setBounds(10, 131, 46, 14);
		contentPane.add(lblClient);
		
		txtIdClient = new JTextField();
		txtIdClient.setEnabled(false);
		txtIdClient.setEditable(false);
		txtIdClient.setText("1");
		txtIdClient.setBounds(339, 127, 35, 23);
		contentPane.add(txtIdClient);
		txtIdClient.setColumns(10);
		
		JLabel lblFechaComanda = new JLabel("Fecha Comanda");
		lblFechaComanda.setBounds(10, 169, 88, 14);
		contentPane.add(lblFechaComanda);
		
		txtDataComanda = new JTextField();
		txtDataComanda.setEnabled(false);
		txtDataComanda.setBounds(119, 166, 186, 20);
		contentPane.add(txtDataComanda);
		txtDataComanda.setColumns(10);
		
		txtDataEntrega = new JTextField();
		txtDataEntrega.setEnabled(false);
		txtDataEntrega.setColumns(10);
		txtDataEntrega.setBounds(119, 195, 186, 20);
		contentPane.add(txtDataEntrega);
		
		JLabel lblFechaEntrefa = new JLabel("Fecha Entrega");
		lblFechaEntrefa.setBounds(10, 198, 88, 14);
		contentPane.add(lblFechaEntrefa);
		
		panel = new JPanel();
		panel.setBackground(new Color(0, 255, 255));
		panel.setForeground(new Color(128, 0, 0));
		panel.setBorder(new EmptyBorder(0, 0, 0, 0));
		panel.setBounds(415, 131, 190, 75);
		contentPane.add(panel);
		
		Pendiente = new JRadioButton("Pendiente");
		Pendiente.setBackground(new Color(0, 255, 255));
		Pendiente.setEnabled(false);
		panel.add(Pendiente);
		
		Preparada = new JRadioButton("Preparada");
		Preparada.setBackground(new Color(0, 255, 255));
		Preparada.setEnabled(false);
		panel.add(Preparada);
		
		Transport = new JRadioButton("Transport");
		Transport.setBackground(new Color(0, 255, 255));
		Transport.setEnabled(false);
		panel.add(Transport);
		
		Lliurada = new JRadioButton("Lliurada");
		Lliurada.setBackground(new Color(0, 255, 255));
		Lliurada.setEnabled(false);
		panel.add(Lliurada);
		ButtonGroup grupo1 = new ButtonGroup();
		grupo1.add(Pendiente);
		grupo1.add(Preparada);
		grupo1.add(Transport);
		grupo1.add(Lliurada);
		
		JButton btnEditar = new JButton("Editar");
		btnEditar.setForeground(new Color(0, 255, 255));
		btnEditar.setBackground(new Color(0, 0, 255));
	
		btnEditar.setEnabled(false);
		btnEditar.setBounds(111, 52, 89, 23);
		contentPane.add(btnEditar);
	
		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.setForeground(new Color(0, 255, 255));
		btnEliminar.setBackground(new Color(0, 0, 255));
		
		
		btnEliminar.setEnabled(false);
		btnEliminar.setBounds(200, 52, 89, 23);
		contentPane.add(btnEliminar);
		lblEstadi = new JLabel("Estado");
		lblEstadi.setBackground(new Color(0, 0, 128));
		lblEstadi.setBounds(495, 104, 46, 14);
		contentPane.add(lblEstadi);
		txtTextoPorDefecto = new JTextField();
		txtTextoPorDefecto.setEditable(false);
		txtTextoPorDefecto.setVisible(false);
		txtTextoPorDefecto.setEnabled(false);
		txtTextoPorDefecto.setText("Texto Por defecto\r\n");
		txtTextoPorDefecto.setColumns(10);
		txtTextoPorDefecto.setBounds(119, 128, 186, 23);
		contentPane.add(txtTextoPorDefecto);
		
		textField_1 = new JTextField();
		textField_1.setEnabled(false);
		textField_1.setEditable(false);
		textField_1.setVisible(false);
		textField_1.setColumns(10);
		textField_1.setBounds(119, 166, 186, 20);
		contentPane.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setEnabled(false);
		textField_2.setEditable(false);
		textField_2.setVisible(false);
		textField_2.setColumns(10);
		textField_2.setBounds(119, 197, 186, 20);
		contentPane.add(textField_2);
		JButton btnNewButton = new JButton("Buscar");
		btnNewButton.setForeground(new Color(0, 255, 255));
		btnNewButton.setBackground(new Color(0, 0, 255));
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.setBackground(new Color(0, 0, 255));
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnGuardar.setVisible(false);
				btnGuardar.setEnabled(false);
				btnEliminar.setEnabled(true);
				btnNewButton.setEnabled(true);
				txtIdClient.setEnabled(false);
				txtIdClient.setEditable(false);
				txtDataComanda.setEnabled(false);
				txtDataEntrega.setEnabled(false);
				txtNomClient.setEnabled(false);
				Pendiente.setEnabled(false);
				Lliurada.setEnabled(false);
				Preparada.setEnabled(false);
				Transport.setEnabled(false);
				txtTextoPorDefecto.setEditable(false);
				txtTextoPorDefecto.setVisible(false);
				txtTextoPorDefecto.setEnabled(false);
				textField_1.setEnabled(false);
				textField_1.setEditable(false);
				textField_1.setVisible(false);
				textField_2.setEnabled(false);
				textField_2.setEditable(false);
				textField_2.setVisible(false);
		
				txtDataComanda.setVisible(true);
				txtDataEntrega.setVisible(true);
				txtNomClient.setVisible(true);
				txtNomClient.setVisible(false);
				Client	cliente= new Client();
				cliente.setNomClient(txtTextoPorDefecto.getText());
				Comanda cosa = new Comanda();
				DateFormat format = new SimpleDateFormat("d MMM u k z yyyy");
						cosa.setClient(cliente);
						 Date date=new Date();
						 Date date2=new Date();
						try {
							date = format.parse(textField_1.toString());
							date2 = format.parse(textField_2.toString());
							cosa.setDataComanda(date);
							cosa.setDataLliurament(date2);
						} catch (ParseException e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
						}
						cosa.setIdComanda(Integer.valueOf(txt_IdComanda.toString()));
						Lliurada.setEnabled(true);
						Preparada.setEnabled(true);
						Transport.setEnabled(true);
						if(Lliurada.isSelected()) {//* is= es
							cosa.setEstat(ComandaEstat.LLIURADA);
						}
						else if(Pendiente.isSelected()) {//* is= es
							cosa.setEstat(ComandaEstat.PENDENT);
						}
						else if(Preparada.isSelected()) {//* is= es
							cosa.setEstat(ComandaEstat.PREPARADA);
						}
						else if(Transport.isSelected()) {//* is= es
							cosa.setEstat(ComandaEstat.TRANSPORT);
						}
			}
		});
		btnGuardar.setEnabled(false);
		btnGuardar.setBounds(200, 20, 89, 23);
		contentPane.add(btnGuardar);
		btnGuardar.setVisible(false);
		btnNewButton.addActionListener(new ActionListener() {
			boolean trobat = false;
			public void actionPerformed(ActionEvent e) {
				int i=0;
	
				for(int xd=0;xd<dtm.getRowCount();xd++) {
					dtm.removeRow(xd);
				}
				for(Comanda c: elMeuMagatzem.getComandes()) {
					btnEditar.setEnabled(true);
					btnEliminar.setEnabled(true);
					if (String.valueOf(c.getIdComanda()).equals( txt_IdComanda.getText())) {
						txtDataComanda.setText( c.getDataComanda().toString());
					
						txtDataEntrega.setText( c.getDataLliurament().toString());
						txtNomClient.setText(c.getClient().getNomClient());
						txtIdClient.setText(String.valueOf(c.getClient().getIdClient()));
						trobat = true;
						if(c.getEstat()==ComandaEstat.PENDENT) {
							Pendiente.setSelected(true);
						}
						else if(c.getEstat()==ComandaEstat.LLIURADA) {
							Lliurada.setSelected(true);
						}
						else if(c.getEstat()==ComandaEstat.PREPARADA) {
							Preparada.setSelected(true);
						}
						else if(c.getEstat()==ComandaEstat.TRANSPORT) {
							Transport.setSelected(true);
						}
						Object[] comanda = new Object [6];
						int suma =0;
						for(int x=0;x<c.getLinies().size();x++) {
							comanda[1]=c.getLinies().get(x).getProducte().getNomProducte();
							comanda[2]=c.getLinies().get(x).getPreuVenda();
							comanda[3]=c.getLinies().get(x).getQuantitat();			
							comanda[0]=suma+ c.getLinies().get(x).getPreuVenda();	
							dtm.addRow(comanda);
						}
						table.setModel(dtm);
					
						
						
							
							
							btnEditar.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent arg0) {
									btnGuardar.setVisible(true);
									DateFormat format = new SimpleDateFormat("d MMM u k z yyyy");
									
									table.setEnabled(true);
									btnGuardar.setEnabled(true);
									btnEliminar.setEnabled(false);
									btnNewButton.setEnabled(false);
									txtDataComanda.setEnabled(true);
									txtDataEntrega.setEnabled(true);
									txtNomClient.setEnabled(true);
									Pendiente.setEnabled(true);
									Lliurada.setEnabled(true);
									Preparada.setEnabled(true);
									Transport.setEnabled(true);
									table_1.setEnabled(true);
									Date date3;
									try {
										date3 = format.parse(txtDataComanda.toString());
										 Date date2 = format.parse(txtDataEntrega.toString());
										c.setDataComanda(date3);
										c.setDataLliurament(date2);
									} catch (ParseException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									c.getClient().setNomClient(txtNomClient.toString());
									c.getClient().setIdClient(Integer.parseInt(txtIdClient.toString()));
									if(Lliurada.isSelected()) {//* is= es
										c.setEstat(ComandaEstat.LLIURADA);
									}
									else if(Pendiente.isSelected()) {//* is= es
										c.setEstat(ComandaEstat.PENDENT);
									}
									else if(Preparada.isSelected()) {//* is= es
										c.setEstat(ComandaEstat.PREPARADA);
									}
									else if(Transport.isSelected()) {//* is= es
										c.setEstat(ComandaEstat.TRANSPORT);
									}
									
									
								}
							});
							
					
						
					}
					btnEliminar.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							c.setDataComanda(null);
							c.setDataLliurament(null);
							c.setEstat(null);
							c.setLinies(null);
							
						}
					});


				if (!trobat) {
					txtDataComanda.setText(null);
					txtDataEntrega.setText(null);
					txtNomClient.setText(null);
					txtIdClient.setText(null);
				}
			}
			}});
		btnNewButton.setBounds(20, 52, 89, 23);
		contentPane.add(btnNewButton);
		
		txtNomClient = new JTextField();
		txtNomClient.setEnabled(false);
		txtNomClient.setText("1");
		txtNomClient.setColumns(10);
		txtNomClient.setBounds(119, 127, 170, 23);
		contentPane.add(txtNomClient);
		
		table = new JTable();
		table.setForeground(new Color(0, 0, 0));
		table.setBackground(Color.WHITE);
		table.setEnabled(false);
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Importe", "Cantidad", "Nombre Producto", "Precio Venta"
			}
		));
		table.setBounds(30, 238, 543, 346);
		contentPane.add(table);
		
		table_1 = new JTable();
		table_1.setBackground(new Color(0, 255, 255));
		table_1.setBorder(new CompoundBorder(null, new BevelBorder(BevelBorder.LOWERED, null, null, null, null)));
		table_1.setForeground(new Color(0, 0, 255));
		table_1.setEnabled(false);
		table_1.setBounds(30, 225, 543, 14);
		
		table_1.setModel(new DefaultTableModel(
			new Object[][] {
				{"Importe", "Cantidad", "Nombre Producto", "Precio Venta"},
			},
			new String[] {
				"Importe", "Cantidad", "Nombre Producto", "Precio Venta"
			}
		));
		contentPane.add(table_1);
		
		JButton A�adir = new JButton("A�adir Comanda ");
		A�adir.setForeground(new Color(0, 255, 255));
		A�adir.setBackground(new Color(0, 0, 255));
		A�adir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Interficie.main(elMeuMagatzem);
	
			}
		});
		A�adir.setBounds(30, 591, 151, 23);
		contentPane.add(A�adir);
		
		JButton Editar = new JButton("A�adir Productos");
		Editar.setForeground(new Color(0, 255, 255));
		Editar.setBackground(new Color(0, 0, 255));
		Editar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			contentPane.setEnabled(false);
				Interficie3.main(elMeuMagatzem,contentPane);
				
			}
		});
		Editar.setBounds(430, 591, 143, 23);
		contentPane.add(Editar);
		
		
		
		
	
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	
}
