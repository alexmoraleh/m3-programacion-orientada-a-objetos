package ventanas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;

import tastat.Client;
import tastat.Magatzem;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.border.BevelBorder;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Image;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.print.DocFlavor.URL;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;

public class Managecltwd {

	private JFrame frame;
	private JTable table;
	private JTable table_1;
	private Frame framePadre;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Managecltwd window = new Managecltwd();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the application.
	 * @return 
	 */
	/*public Managecltwd() {
		frame = new JFrame();
	}*/
	public Managecltwd(Magatzem m, int i, Frame framePadre) { //Hacer metodos para crear y otro para modificar el cliente. Hacer diferentes constructores
		//super();
		this.framePadre = framePadre;
		modificar(m,i);
		frame.setVisible(true);
	}
	public Managecltwd(Magatzem m,Frame f) {//Quitar un constructor para que funcione Design
		this.framePadre=f;
		add(m, f);
		frame.setVisible(true);
	}
	
	public void add(Magatzem m, Frame f) {
		frame=new JFrame();
		frame.setBounds(100, 100, 822, 196);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setTitle("A�adir Cliente");
		JLabel lblModificarClient = new JLabel("A�adir Cliente");
		lblModificarClient.setFont(new Font("Arial", Font.PLAIN, 20));
		lblModificarClient.setBounds(26, 11, 150, 49);
		frame.getContentPane().add(lblModificarClient);
		DefaultTableModel dtm = new DefaultTableModel(
				new Object[][] {
					null
				},
				new String[] {
						"ID", "Nombre", "CIF", "Activo", "Direcci\u00F3n", "Poblaci\u00F3n", "Pais", "Contacto", "Telefono", "Latitud", "Longitud"
					}
				){private static final long serialVersionUID = 1L;
				@SuppressWarnings("rawtypes")
				Class[] columnTypes = new Class[] {
						Integer.class, String.class, Object.class, Boolean.class, Object.class, Object.class, Object.class, Object.class, Object.class, Double.class, Double.class
					};
					public Class getColumnClass(int columnIndex) {
						return columnTypes[columnIndex];
					}
				};
		table = new JTable(dtm);
		table.setBounds(26, 85, 763, 16);
		table.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		frame.getContentPane().add(table);
		table_1 = new JTable(new DefaultTableModel(
				new Object[][] {
					{"ID", "Nombre", "CIF", "Activo", "Direcci\u00F3n", "Poblaci\u00F3n", "Pais", "Contacto", "Telefono", "Latitud", "Longitud"},
				},
				new String[] {
						"ID", "Nombre", "CIF", "Activo", "Direcci\u00F3n", "Poblaci\u00F3n", "Pais", "Contacto", "Telefono", "Latitud", "Longitud"
					}
				));
		table_1.setEnabled(false);
		table_1.setBackground(SystemColor.window);
		table_1.setBounds(26, 71, 764, 15);
		frame.getContentPane().add(table_1);
		JButton btnAcept = new JButton("Guardar");
		btnAcept.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(((Vector)dtm.getDataVector().elementAt(0)).get(0)==null||((Vector)dtm.getDataVector().elementAt(0)).get(1)==null) {
					JOptionPane.showMessageDialog(frame, "A�ade datos");
				}
				else {
					Client c=new Client();
					Vector v=(Vector)dtm.getDataVector().elementAt(0);
					c.setIdClient((int)v.get(0));
					c.setNomClient((String)v.get(1));
					c.setCIF((String)v.get(2));
					c.setActiu((Boolean)v.get(3));
					c.setDireccio((String)v.get(4));
					c.setPoblacio((String)v.get(5));
					c.setPais((String)v.get(6));
					c.setPersonaContacte((String)v.get(7));
					c.setTelefon((String)v.get(8));
					c.setLatitud((Double)v.get(9));
					c.setLongitud((Double)v.get(10));
					m.getClients().add(c);
					framePadre.setEnabled(true);
					frame.dispose();
				}
			}
		});
		btnAcept.setBounds(233, 123, 89, 23);
		frame.getContentPane().add(btnAcept);
		
		JButton btnCanelar = new JButton("Cancelar");
		btnCanelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				framePadre.setEnabled(true);
				frame.dispose();
			}
		});
		btnCanelar.setBounds(456, 123, 89, 23);
		frame.getContentPane().add(btnCanelar);
		frame.addWindowListener(new java.awt.event.WindowAdapter() {
	        public void windowClosing(WindowEvent winEvt) {
	        	framePadre.setEnabled(true);
				frame.dispose();
	        }
	    });
	}
	
	public void modificar(Magatzem m, int i) {
		frame = new JFrame();
		frame.setBounds(100, 100, 822, 196);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setTitle("Modificar Cliente");
		/*URL iconURL = new URL("logo.ico"); getClass().getResource("logo.ico");
		ImageIcon icon = new ImageIcon(iconURL);
		frame.setIconImage(icon.getImage());*/
		Image icon = Toolkit.getDefaultToolkit().getImage("logo.ico");
	    frame.setIconImage(icon);
		JLabel lblModificarClient = new JLabel("Modificar Cliente");
		lblModificarClient.setFont(new Font("Arial", Font.PLAIN, 20));
		lblModificarClient.setBounds(26, 11, 150, 49);
		frame.getContentPane().add(lblModificarClient);
		DefaultTableModel dtm = new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
						"ID", "Nombre", "CIF", "Activo", "Direcci\u00F3n", "Poblaci\u00F3n", "Pais", "Contacto", "Telefono", "Latitud", "Longitud"
					}
				){private static final long serialVersionUID = 1L;
				@SuppressWarnings("rawtypes")
				Class[] columnTypes = new Class[] {
						Integer.class, String.class, Object.class, Boolean.class, Object.class, Object.class, Object.class, Object.class, Object.class, Double.class, Double.class
					};
					public Class getColumnClass(int columnIndex) {
						return columnTypes[columnIndex];
					}
				};
		table = new JTable(dtm);
		table.setBounds(26, 85, 764, 16);
		table.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		frame.getContentPane().add(table);
		table_1 = new JTable(new DefaultTableModel(
				new Object[][] {
					{"ID", "Nombre", "CIF", "Activo", "Direcci\u00F3n", "Poblaci\u00F3n", "Pais", "Contacto", "Telefono", "Latitud", "Longitud"},
				},
				new String[] {
						"ID", "Nombre", "CIF", "Activo", "Direcci\u00F3n", "Poblaci\u00F3n", "Pais", "Contacto", "Telefono", "Latitud", "Longitud"
					}
				));
		table_1.setEnabled(false);
		table_1.setBackground(SystemColor.window);
		table_1.setBounds(26, 71, 764, 15);
		frame.getContentPane().add(table_1);
		
		JButton btnAcept = new JButton("Guardar");
		btnAcept.addActionListener(new ActionListener() {
			@SuppressWarnings("rawtypes")
			public void actionPerformed(ActionEvent arg0) {
				Client cl=null;
				for(Client c:m.getClients()) {
					if(c.getIdClient()==i) cl=c;
				}
				Client c=new Client();
				Vector v=(Vector)dtm.getDataVector().elementAt(0);
				c.setIdClient((int)v.get(0));
				c.setNomClient((String)v.get(1));
				c.setCIF((String)v.get(2));
				c.setActiu((Boolean)v.get(3));
				c.setDireccio((String)v.get(4));
				c.setPoblacio((String)v.get(5));
				c.setPais((String)v.get(6));
				c.setPersonaContacte((String)v.get(7));
				c.setTelefon((String)v.get(8));
				c.setLatitud((Double)v.get(9));
				c.setLongitud((Double)v.get(10));
				m.getClients().remove(cl);
				m.getClients().add(c);
				//vec=(Vector) dtm.getDataVector().elementAt(0);
				framePadre.setEnabled(true);
				frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
				frame.dispose();
			}
		});
		btnAcept.setBounds(233, 123, 89, 23);
		frame.getContentPane().add(btnAcept);
		
		JButton btnCanelar = new JButton("Cancelar");
		btnCanelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				framePadre.setEnabled(true);
				frame.dispose();
			}
		});
		btnCanelar.setBounds(456, 123, 89, 23);
		frame.getContentPane().add(btnCanelar);
		int x=0;
		for(Client c:m.getClients()) {
			if(c.getIdClient()==i) dtm.addRow(m.getClients().get(x).getTodo().toArray());
			x++;
		}
		frame.addWindowListener(new java.awt.event.WindowAdapter() {
	        public void windowClosing(WindowEvent winEvt) {
	        	framePadre.setEnabled(true);
				frame.dispose();
	        }
	    });
	}
}
