package ventanas;

import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import java.awt.Dimension;

import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;
import tastat.*;

public class Window2 {

	private JFrame frame;
	private JTable table;
	private JTable table_1;
	static DefaultTableModel dtm = new DefaultTableModel(new Object[][] {}, new String[] { "idProveidor", "nom" });

	private static ArrayList<Proveidor> proveidors = new ArrayList<>();
	static Random rnd = new Random();

	/**
	 * Launch the application.
	 */
	public static void newScreen(JTextField prov, Magatzem mgz) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window2 window = new Window2(prov, mgz);

					while (dtm.getRowCount() > 0)
						dtm.removeRow(0);

					Object[] composicio = new Object[2];
					for (Proveidor s : proveidors) {
						composicio[0] = s.getIdProveidor();
						composicio[1] = s.getNomProveidor();
						dtm.addRow(composicio);
					}

					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Window2(JTextField prov, Magatzem mgz) {

		initialize(prov, mgz);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(JTextField prov, Magatzem mgz) {
		frame = new JFrame();
		frame.setTitle("llista_prov");
		frame.getContentPane().setBackground(SystemColor.activeCaption);
		frame.setBackground(SystemColor.activeCaption);
		frame.setBounds(100, 100, 450, 300);
		frame.getContentPane().setLayout(null);

		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation(dim.width / 2 - frame.getSize().width / 2, dim.height / 2 - frame.getSize().height / 2);

		table = new JTable();
		table.setModel(dtm);
		table.setBounds(10, 28, 414, 186);

		/*
		 * for(int i = 0; i < mgz.getProveidors();i++){
		 * 
		 * }
		 */
		frame.getContentPane().add(table);

		table_1 = new JTable();
		table_1.setEnabled(false);
		DefaultTableModel dtm = new DefaultTableModel(new Object[][] { { null, "idProveidor", "nom" }, },
				new String[] { "", "idProveidor", "nom" });
		table_1.setModel(dtm);
		table_1.getColumnModel().getColumn(0).setPreferredWidth(151);
		table_1.getColumnModel().getColumn(1).setPreferredWidth(226);
		table_1.getColumnModel().getColumn(2).setPreferredWidth(245);
		table_1.setBounds(10, 11, 414, 16);
		frame.getContentPane().add(table_1);
		
		Object[] proveidors = new Object[2];
		for(Proveidor pr:mgz.getProveidors()) {
			proveidors[0] = pr.getIdProveidor();
			proveidors[1] = pr.getNomProveidor();
			dtm.addRow(proveidors);
		}
		
		JButton btnCancelar = new JButton("CANCELAR");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		btnCancelar.setBounds(327, 227, 97, 23);
		frame.getContentPane().add(btnCancelar);

		JButton btnAceptar = new JButton("ACEPTAR");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (table.getSelectedRow() != -1) {
					Object proveidor = new Object();
					int id = idProveidor(proveidor);
					System.out.println(id);
					prov.setText(String.valueOf(id));
					frame.dispose();
				} else {
					JOptionPane.showMessageDialog(frame, "No has seleccionat ningun component");
				}
			}
		});
		btnAceptar.setBounds(192, 227, 102, 23);
		frame.getContentPane().add(btnAceptar);
	}

	public int idProveidor(Object proveidor) {
		proveidor = dtm.getDataVector().elementAt(table.getSelectedRow());
		String[] prov = proveidor.toString().split(",");
		String idProv = prov[0];
		idProv = idProv.replaceAll("\\[", "");
		return Integer.parseInt(idProv);
	}

}
