package ventanas;

import java.awt.EventQueue;

import tastat.Client;
import tastat.Comanda;
import tastat.ComandaEstat;
import tastat.ComandaLinia;
import tastat.Magatzem;
import tastat.Producte;

import javax.swing.JFrame;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import java.awt.Color;
import java.awt.Button;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Interficie3 {
	private DefaultTableModel dtm = new DefaultTableModel(
			new Object[][] { { "Importe", "Cantidad", "Nombre Producto", "Precio Venta" }, },
			new String[] { "Importe", "Cantidad", "Nombre Producto", "Precio Venta" });
	private JFrame frame;

	/**
	 * Launch the application.
	 * 
	 * @param contentPane
	 */
	public static void main(Magatzem elMeuMagatzem, JPanel contentPane) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interficie3 window = new Interficie3(elMeuMagatzem);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interficie3(Magatzem elMeuMagatzem) {

		initialize(elMeuMagatzem);

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(Magatzem elMeuMagatzem) {

		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(0, 255, 255));
		frame.setBounds(100, 100, 228, 155);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(109, 8, 86, 20);
		frame.getContentPane().add(comboBox);

		JLabel lblId = new JLabel("ID Comanda");
		lblId.setBounds(10, 11, 89, 14);
		frame.getContentPane().add(lblId);

		JLabel lblIdProducte = new JLabel("Nom Producte");
		lblIdProducte.setBounds(10, 36, 89, 14);

		frame.getContentPane().add(lblIdProducte);

		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBounds(109, 39, 86, 20);
		frame.getContentPane().add(comboBox_1);
		for (Producte d : elMeuMagatzem.getProductes()) {
			comboBox_1.addItem(d.getNomProducte());
		}
		for (Comanda d : elMeuMagatzem.getComandes()) {
			comboBox.addItem(d.getIdComanda());
		}
		Button button = new Button("A\u00F1adir");
		button.setForeground(new Color(0, 255, 255));
		button.setBackground(new Color(0, 0, 255));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				for (Comanda d : elMeuMagatzem.getComandes()) {
					if (d.getIdComanda() == (Integer) comboBox.getSelectedItem()) {
						System.out.println("Esto Funka we"+(Integer) comboBox.getSelectedItem());
						for (Producte d2 : elMeuMagatzem.getProductes()) {
							if (d2.getNomProducte().equals( (String)comboBox_1.getSelectedItem())){
								System.out.println("Esto Funka we"+"2");
								d.getLinies().add(new ComandaLinia(d2, d2.getStock(), d2.getPreuVenda()));
							
						
							}
						}
					}

				}
			}
		});
		button.setBounds(69, 84, 70, 22);
		frame.getContentPane().add(button);

	}
}
