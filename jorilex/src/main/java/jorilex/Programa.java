package jorilex;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Programa {
	static Scanner rd = new Scanner(System.in);
	static int id = 0;

	public static void main(String[] args) {

		Connection con = null;
		try {

			// 1- CONNEXI�
			// con =
			// DriverManager.getConnection("jdbc:mysql://localhost:3306/demo?user=root&password=usbw");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/ttb?user=root&password=super3");
			int men = 1;
			while (men != 0) {
				men = Menu();
				switch (men) {
				case 1:
					Informaciondelabasededatos(con);
					break;
				case 2:
					verDatos(con);
					break;
				case 3:
					generarComanda();
					break;
				case 4:
					verComanda();
					break;
				case 5:
					generarInforme();
					break;
				case 6:
					veureInforme();
					break;
				case 7:
					prepararComandas();
					break;
				case 8:
					generarOrdresCompra();
					break;
				case 9:
					veureOrdresCompra();
					break;
				case 10:
					veureDiariMoviments();
					break;
				default:
					System.out.println("Adi�s!");
					break;
				}
			}
		} catch (SQLException e) {

			System.err.println("Error de abertura de conexi�n: " + e.getMessage());
			e.printStackTrace();

		} finally {

			if (con != null)
				try {

					con.close();

				} catch (SQLException e) {
					System.err.println("Error al cerrar la conexi�n: " + e.getMessage());
					e.printStackTrace();
				}
		}
	}

	public static int Menu() {
		System.out.println("1. Ver tablas y Columnas.");
		System.out.println("2. Veure datos.");
		System.out.println("3. Generar comanda.");
		System.out.println("4. Ver comanda por cliente");
		System.out.println("5. Generar informe situaci�n.");
		System.out.println("6. Ver informe situaci�n.");
		System.out.println("7. Preparar comandas.");
		System.out.println("8. Generar ordrenes de compra.");
		System.out.println("9. Ver ordrenes de compra.");
		System.out.println("10. Ver diario de movimientos.");
		System.out.println("0. Salir");
		System.out.print("Escoja una opci�n: ");
		return rd.nextInt();
	}

	// Parte Alex

	public static void generarComanda() throws SQLException {
		Statement st = DB.connection().createStatement();
		ResultSet rs = st.executeQuery("SELECT idClient, nomClient FROM client;");
		while (rs.next()) {
			System.out.println("IDClient: " + rs.getString("idClient") + ", NomClient: " + rs.getString("nomClient"));
		}
		System.out.print("Escoja un ID de un cliente: ");
		int idClient = rd.nextInt();
		iv(st, rs, idClient);
	}

	public static void iv(Statement st, ResultSet rs, int idClient) throws SQLException {
		
			rs = st.executeQuery(
					"SELECT idProducte, nomProducte, preuVenda, stock FROM producte WHERE tipusProducte like \"VENDIBLE\" ");
			while (rs.next()) {
				System.out
						.println("IDProducte: " + rs.getInt("idProducte") + ", NomProducte: " + rs.getString("nomProducte")
								+ ", PreuVenda: " + rs.getDouble("preuVenda") + ", Stock: " + rs.getInt("stock"));
			}
			System.out.print("Escoja un ID de un produto: ");
			int idProducte = rd.nextInt();
			System.out.println("Coloque la quantidad: ");
			int cantidad = rd.nextInt();
			System.out.println("Coloque el precio por unidad (0 para valor predeterminado): ");
			double precio = rd.nextDouble();
			if (precio <= 0) {
				rs = st.executeQuery("SELECT preuVenda FROM producte WHERE idProducte like '" + idProducte + "';");
				rs.next();
				double aux = rs.getDouble("preuVenda");
				precio = aux * cantidad;
			}
	
			else
				precio = precio * cantidad;
			System.out.println("Precio final " + precio);
			
			// Date d=new Date();
			PreparedStatement ps;
			ps = DB.connection().prepareStatement("INSERT INTO comanda (idClient, dataComanda, dataLliurament) "
					+ "VALUES ('" + idClient + "', SYSDATE(), SYSDATE());");
			ps.executeUpdate();
			rs = st.executeQuery("SELECT max(idComanda) as count from comanda");
			rs.next();
			int x = rs.getInt("count");
			rs = st.executeQuery("SELECT count(*) as count from comanda_linia WHERE idComanda LIKE " + x + ";");
			rs.next();
			int count = rs.getInt("count");
			ps = DB.connection()
					.prepareStatement("INSERT INTO comanda_linia (idComanda, idLinia, idProducte, quantitat, preuVenda) "
							+ "VALUES ('" + x + "','" + (count + 1) + "','" + idProducte + "','" + cantidad + "','" + precio
							+ "');");
			ps.executeUpdate();
			rs = st.executeQuery("SELECT stock from producte where idProducte like '" + idProducte + "';");
			rs.next();
			int stock = rs.getInt("stock");
			if(stock > cantidad) {
				ps = DB.connection().prepareStatement(
						"UPDATE producte SET stock = " + (stock - cantidad) + " where idProducte like '" + idProducte + "';");
				ps.executeUpdate();
			} else {
				System.out.println("La cantidad es superior al stock.");
			}		
	}

	public static void verComanda() throws SQLException {
		Statement st = DB.connection().createStatement();
		ResultSet rs = st.executeQuery(
				"SELECT idClient, nomClient FROM client where idClient in (SELECT idClient from comanda)");
		System.out.println();
		while (rs.next()) {
			System.out.println("IDClient: " + rs.getString("idClient") + ", NomClient: " + rs.getString("nomClient"));
		}
		System.out.print("Escoja un ID de un cliente: ");
		int idClient = rd.nextInt();
		rs = st.executeQuery("SELECT nomClient from client where idClient = " + idClient);
		rs.next();
		System.out.println();
		System.out.println("Comandas de " + rs.getString("nomClient") + ": ");
		rs = st.executeQuery("SELECT idComanda FROM comanda where idClient = " + idClient);
		while (rs.next()) {
			System.out.println(rs.getInt("idComanda"));
		}
		System.out.print("Escoja una comanda: ");
		int com = rd.nextInt();
		rs = st.executeQuery("SELECT dataComanda, dataLliurament, estatComanda, ports FROM comanda where idClient = "
				+ idClient + " and idComanda = " + com);
		rs.next();
		System.out.println("Datos de la comanda: ");
		System.out.println("Fecha de la comanda: " + rs.getDate("dataComanda") + "\n" + "Fecha de entrega: "
				+ rs.getDate("dataLliurament") + "\n" + "Estado de la comanda: " + rs.getString("estatComanda") + "\n"
				+ "Puertos: " + rs.getInt("ports"));
		System.out.println();
		rs = st.executeQuery("SELECT quantitat, preuVenda from comanda_linia where idComanda = " + com);
		while (rs.next()) {
			com = com + (rs.getInt("quantitat") * rs.getInt("preuVenda"));
		}
		System.out.println("Precio total: " + com + "�");
	}

	enum estatComanda {
		PENDENT, PREPARADA, TRANSPORT, LLIURADA;
	}

	// Jose
	public static void verDatos(Connection con) throws SQLException {


		ResultSet resultadoConsulta;
		ResultSetMetaData metaDatos;
		String[][] datosDevueltos;
		String[] nombresColumnas;
		int j = 0;
		int x=0;
		int i=0;
		Statement sentencia = con.createStatement();
		String Tablas[] = { "direccio", "comanda_linia", "proveidor", "tarifa", "client", "diari_moviments", "comanda",
				"producte", "producte_lot", "producte_composicio", "ordre_compra" };
		String Tablas2[] = { "0.direccio", "1.comanda_linia", "2.proveidor", "3.tarifa", "4.client", "5.diari_moviments", "6.comanda",
				"7.producte", "8.producte_lot", "9.producte_composicio", "10.ordre_compra" };
		for( i=0;i<11;i++) {
			System.out.println(Tablas2[i]);
		}
		System.out.println("Elige una tabla (por el numero)");
		i=rd.nextInt();


			String q = "select * from" + " " + Tablas[i];
			resultadoConsulta = sentencia.executeQuery(q);

			metaDatos = resultadoConsulta.getMetaData();
			if (i==0) {
				System.out.println("idDireccio" + " " + "Dirreccio" + " " + "Poblacio" + " " + "Pais" + " " + " Latitud"
						+ " " + "Longitud");

				while (resultadoConsulta.next()) {

					System.out.println(resultadoConsulta.getInt(1) + " " + resultadoConsulta.getString(2) + " "
							+ resultadoConsulta.getString(3) + " " + resultadoConsulta.getString(4) + " "
							+ resultadoConsulta.getDouble(5) );
				}
			} else if (i==1) {
				System.out.println("idComanda" + " " + "idLinia" + " " + "idProducte" + " " + "Pais" + " "
						+ " quantitat" + " " + "preuVenda");

				while (resultadoConsulta.next()) {

					System.out.println(resultadoConsulta.getInt(1) + " " + resultadoConsulta.getInt(2) + " "
							+ resultadoConsulta.getInt(3) + " " + resultadoConsulta.getInt(4) + " "
							+ resultadoConsulta.getInt(5));
				}
			} else if (i==2) {

				System.out.println("idProveidor" + " " + "nomProveidor" + " " + "idDireccio");
				while (resultadoConsulta.next()) {

					System.out.println(resultadoConsulta.getInt(1) + " " + resultadoConsulta.getString(2) + " "
							+ resultadoConsulta.getInt(3));
				}
			} else if (i==3) {
				System.out.println("desde" + " " + "fins" + " " + "preu");
				while (resultadoConsulta.next()) {

					System.out.println(resultadoConsulta.getInt(1) + " " + resultadoConsulta.getInt(2) + " "
							+ resultadoConsulta.getInt(3));
				}
			} else if (i==4) {
				System.out.println("idClient" + " " + "nomClient" + " " + "actiu" + " " + " idDireccio");
				while (resultadoConsulta.next()) {

					System.out.println(resultadoConsulta.getInt(1) + " " + resultadoConsulta.getString(2) + " "
							+ resultadoConsulta.getBoolean(3) + " " + resultadoConsulta.getInt(4));
				}
			} else if (i==5) {
				System.out.println("idMoviment" + " " + "moment" + " " + "idProducte" + " " + " tipusMoviment" + " "
						+ "quantitat" + " " + "observacions" + " " + "idComanda" + " " + "idOrdre" + " " + "idLot");
				while (resultadoConsulta.next()) {

					System.out.println(resultadoConsulta.getInt(1) + " " + resultadoConsulta.getString(2) + " "
							+ resultadoConsulta.getInt(3) + " " + resultadoConsulta.getString(4) + " "
							+ resultadoConsulta.getInt(5) + " " + 
							resultadoConsulta.getString(6) + " "
							+ resultadoConsulta.getString(7) + " " + resultadoConsulta.getInt(8) + " "
							+ resultadoConsulta.getInt(9));
				}
			} else if (i==6) {
				System.out.println("idComanda" + " " + "idClient" + " " + "dataComanda" + " " + " dataLliurament" + " "
						+ "estatComanda");
				while (resultadoConsulta.next()) {

					System.out.println(resultadoConsulta.getInt(1) + " " + resultadoConsulta.getInt(2) + " "
							+ resultadoConsulta.getDate(3) + " " + resultadoConsulta.getDate(4) + " "
							+ resultadoConsulta.getString(5));
				}
			} else if (i==7) {
				System.out.println("idProducte" + " " + "nomProducte" + " " + "descripcioProducte" + " " + " preuVenda"
						+ " " + "stock" + " " + "stockMinim" + " " + "tipusProducte" + " " + "unitatMesura" + " "
						+ "idProveidor" + " " + "tempsFabricacio");
				while (resultadoConsulta.next()) {

					System.out.println(resultadoConsulta.getInt(1) + " " + resultadoConsulta.getString(2) + " "
							+ resultadoConsulta.getString(3) + " " + resultadoConsulta.getInt(4) + ""
							+ resultadoConsulta.getInt(5) + "" + resultadoConsulta.getInt(6) + ""
							+ resultadoConsulta.getString(7) + "" + resultadoConsulta.getString(8) + ""
							+ resultadoConsulta.getInt(9) + resultadoConsulta.getInt(10));
				}
			} else if (i==8) {
				System.out.println("idLot" + " " + "idProducte" + " " + "dataEntrada" + " " + " dataCaducitat" + " "
						+ "quantitat");
				while (resultadoConsulta.next()) {

					System.out.println(resultadoConsulta.getInt(1) + " " + resultadoConsulta.getInt(2) + " "
							+ resultadoConsulta.getDate(3) + " " + resultadoConsulta.getDate(4) + ""
							+ resultadoConsulta.getInt(5));
				}
			} else if (i==9) {
				System.out.println("IdComponent" + " " + "idProducte" + " " + "Quantitat");
				while (resultadoConsulta.next()) {

					System.out.println(resultadoConsulta.getInt(1) + " " + resultadoConsulta.getInt(2) + " "
							+ resultadoConsulta.getInt(3));
				}
			} else if (i==10) {
				System.out.println("idOrdre" + " " + "idProducte" + " " + "idProveidor" + " " + " quantitat" + " "
						+ "dataOrdre" + " " + "dataRecepcio" + " " + "estatOrdre");
				while (resultadoConsulta.next()) {

					System.out.println(resultadoConsulta.getInt(1) + " " + resultadoConsulta.getInt(2) + " "
							+ resultadoConsulta.getInt(3) + " " + resultadoConsulta.getInt(4) + ""
							+ resultadoConsulta.getDate(5) + "" + resultadoConsulta.getString(6));
				}
			}
	}

	public static void Informaciondelabasededatos(Connection con) throws SQLException {

		ResultSet rs;
		ResultSetMetaData metaDatos = null;
		String[][] datosDevueltos;
		String[] nombresColumnas = null;
		String Tablas[] = { "direccio", "comanda_linia", "proveidor", "tarifa", "client", "diari_moviments", "comanda",
				"producte", "producte_lot", "producte_composicio", "ordre_compra" };
		Statement st = con.createStatement();

		ResultSetMetaData rsmd = null;
		String q = null;
		Statement sentencia = con.createStatement();

		for (int j = 0; j < 10; j++) {

			// 2- A partir del resultset obtenim les metadades amb el m�tode getMetaData()
			q="SELECT * FROM " + Tablas[j];
			rs = st.executeQuery(q);
			rsmd = rs.getMetaData();
			// 3- Mostra el numero de columnes de cada registre:
			int nCols = rsmd.getColumnCount();
			System.out.println("Columnes recuperades: " + nCols);

			for (int i = 1; i <= nCols; i++) {

				System.out.println("Columna: " + i + ":");
				System.out.println("  Nom: " + rsmd.getColumnName(i));
				System.out.println("  Tipus: " + rsmd.getColumnTypeName(i));
				System.out.println("  Pot ser nul�la? " + (rsmd.isNullable(i) == 0 ? "No" : "S�"));
				System.out.println("  Amplada m�xima de columna: " + rsmd.getColumnDisplaySize(i));
			}

		}
	}

	// Uri
	// Opcio 5
	public static void generarInforme() throws SQLException {

		// I
		PreparedStatement st = DB.connection().prepareStatement("DROP TABLE IF EXISTS informe;");
		st.executeUpdate();

		// II
		st = DB.connection().prepareStatement("CREATE TABLE informe(" + "id int NOT NULL, " + "idProducte int, "
				+ "nomProducte varchar(255), " + "quantitatDemandada int," + " quantitatExistent int, "
				+ "quantitatAFabricar int, " + "quantitatFabricada int, " + "estat int,  " + "PRIMARY KEY (id));");
		st.executeUpdate();

		// III
		ArrayList<Producte> productes = new ArrayList<Producte>();
		boolean existe = false;
		st = DB.connection().prepareStatement("SELECT * FROM comanda;");
		ResultSet rs = st.executeQuery();

		while (rs.next()) {
			if (rs.getString(5).equals("PENDENT")) {
				st = DB.connection().prepareStatement(
						"SELECT idProducte, quantitat FROM comanda_linia WHERE idComanda=" + rs.getInt(1) + ";");
				ResultSet producte = st.executeQuery();
				while (producte.next()) {
					for (int i = 0; i < productes.size(); i++) {
						if (productes.get(i).getId() == producte.getInt(1)) {
							existe = true;
							productes.get(i).setQuantitat(productes.get(i).getQuantitat() + producte.getInt(2));
							break;
						}
					}

					if (!existe) {
						productes.add(new Producte(producte.getInt(1), rs.getInt(2)));
					}
					existe = false;
				}
			}
		}

		for (int i = 0; i < productes.size(); i++) {
			st = DB.connection().prepareStatement(
					"SELECT nomProducte, stock FROM producte WHERE idProducte=" + productes.get(i).getId());
			ResultSet prod = st.executeQuery();
			prod.next();
			if (productes.get(i).getQuantitat() - prod.getInt(2) > 0) {
				st = DB.connection().prepareStatement(
						"INSERT INTO informe(id, idProducte, nomProducte, quantitatDemandada, quantitatExistent, quantitatAFabricar, quantitatFabricada, estat) VALUES("
								+ id + ", " + productes.get(i).getId() + ", " + "\"" + prod.getString(1) + "\"" + ", "
								+ productes.get(i).getQuantitat() + ", " + "" + prod.getInt(2) + ", "
								+ Math.abs(productes.get(i).getQuantitat() - prod.getInt(2)) + ", 0, 0);");
				st.executeUpdate();
			} else {
				PreparedStatement str = DB.connection().prepareStatement(
						"INSERT INTO informe(id, idProducte, nomProducte, quantitatDemandada, quantitatExistent, quantitatAFabricar, quantitatFabricada, estat) VALUES("
								+ id + ", " + productes.get(i).getId() + ", " + "\"" + prod.getString(1) + "\"" + ", "
								+ productes.get(i).getQuantitat() + ", " + "" + prod.getInt(2) + ", 0 , 0, 1);");
				str.executeUpdate();
			}
			id++;
		}
		System.out.println("Informe creado");
	}

	// Opcio 6
	public static void veureInforme() throws SQLException {
		PreparedStatement st = DB.connection().prepareStatement("SELECT * FROM informe;");
		ResultSet rs = st.executeQuery();
		String print = "";
		while (rs.next()) {
			print += " INFORME " + rs.getInt(1) + " \n";
			print += " ID: " + rs.getInt(1) + "\n IDProducte: " + rs.getInt(2) + "\n Nom producte: " + rs.getString(3)
					+ "\n Quantitat Demandada: " + rs.getInt(4) + "\n Quantitat Existent " + rs.getInt(5)
					+ "\n Quantitat a Fabricar: " + rs.getInt(6) + "\n Quantiat Fabricada: " + rs.getInt(7)
					+ "\n ESTAT: " + rs.getString(8);
		}
		System.out.println(print);
	}

	// Opcio 7
	public static void prepararComandas() throws SQLException {
		boolean end = false;
		DB.connection().setAutoCommit(false);
		PreparedStatement st = DB.connection()
				.prepareStatement("SELECT * FROM comanda WHERE UPPER(estatComanda) LIKE 'PENDENT' ORDER BY idComanda;");
		ResultSet rs = st.executeQuery();
		while (rs.next()) {
			st = DB.connection().prepareStatement(
					"SELECT * FROM comanda_linia WHERE idComanda =" + rs.getInt(1) + " ORDER BY idComanda;");
			ResultSet productes_comanda = st.executeQuery();
			while (productes_comanda.next()) {
				st = DB.connection().prepareStatement(
						"SELECT stock FROM producte WHERE idProducte =" + productes_comanda.getInt(3) + ";");
				ResultSet producte = st.executeQuery();

				producte.next();
				// Actualiza solo la linea del producto correspondiente a la comanda no la
				// comanda entera
				if (producte.getInt(1) - productes_comanda.getInt(4) >= 0) {
					st = DB.connection().prepareStatement(
							"UPDATE producte SET stock =" + (producte.getInt(1) - productes_comanda.getInt(4))
									+ " WHERE idProducte=" + productes_comanda.getInt(3) + ";");
					st.executeUpdate();
					st = DB.connection().prepareStatement(
							"INSERT INTO diari_moviments(idProducte, idComanda, quantitat, tipusMoviment, observacions) VALUES("
									+ productes_comanda.getInt(3) + ", " + productes_comanda.getInt(1) + ", "
									+ productes_comanda.getInt(4) + " ," + "'S'" + "," + "'OBSERVADO'" + ");");
					st.executeUpdate();
				} else {
					System.out.println("NO HAY STOCK SUFICIENTE PARA LA COMANDA CON ID:" + rs.getInt(1));
					end = true;
				}
				
				if(end) {
					System.out.println("TRANSACTION ROLLBACK...");
					DB.connection().rollback();
				} else {
					System.out.println("LA TRANSACTION S'HA FET DE FORMA CORRECTA.");
					DB.connection().commit();
				}
				end = false;
			}
		}
		
		DB.connection().setAutoCommit(true);
	}
	

	// Opcio 8
	public static void generarOrdresCompra() throws SQLException {
		PreparedStatement st = DB.connection().prepareStatement(
		"SELECT idProducte, stock, stockMinim, idProveidor FROM producte WHERE stock < stockMinim AND tipusProducte LIKE 'INGREDIENT';");
		ResultSet rs = st.executeQuery();
		Date avui = new Date();
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
		String avuisql = date.format(avui);
		avuisql = avuisql.replace('-', '/');


		while (rs.next()) {
		String kk = "INSERT INTO ordre_compra(idProducte, quantitat, idProveidor, dataOrdre, dataRecepcio) VALUES("	+ rs.getInt(1) + ", " + (rs.getInt(3) * 2) + ", " + rs.getInt(4) + "," + "CURDATE()" + ", " + "CURDATE()" + ")";
		st = DB.connection().prepareStatement(kk);
		st.executeUpdate();
		}
		System.out.println("Ordenes generadas");
	}
	// Opcio 9
	public static void veureOrdresCompra() throws SQLException {
		PreparedStatement st = DB.connection()
				.prepareStatement("SELECT * FROM ordre_compra ORDER BY idProveidor, idProducte;");
		ResultSet rs = st.executeQuery();
		String print = "";
		while (rs.next()) {
			print += "ORDRE COMPRA " + rs.getInt(1) + "\n";
			print += "ID Ordre: " + rs.getInt(1) + "\n ID Producte:" + rs.getInt(2) + "\n ID Provieodr: " + rs.getInt(3)
					+ "\n Quantitat: " + rs.getInt(4) + "\n"; // Data Ordre: " + rs.getDate(5) + "\n Data Recepcio: "
			// + rs.getDate(6) + "\n Estat Ordre: " + rs.getString(7);
		}
		System.out.println(print);
	}

	// Opcio 10
	public static void veureDiariMoviments() throws SQLException {
		PreparedStatement st = DB.connection().prepareStatement("SELECT * FROM diari_moviments;");
		ResultSet rs = st.executeQuery();
		String print = "";
		while (rs.next()) {
			print += "DIARI MOVIMENTS \n";
			print += "ID Moviments" + rs.getInt(1) + "\n Moment:" + rs.getTimestamp(2) + "\n ID Producte: "
					+ rs.getInt(3) + "\n Tipus moviment: " + rs.getString(4) + "\n Quantitat: " + rs.getInt(5)
					+ "\n Observacions: " + rs.getString(6) + "\n ID Comanda: " + rs.getInt(7) + "\n  ID Ordre: "
					+ rs.getInt(8) + "\n ID Lot: " + rs.getInt(9) + "\n";
		}
		System.out.println(print);
	}

}
