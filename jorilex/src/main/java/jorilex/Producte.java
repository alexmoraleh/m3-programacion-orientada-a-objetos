package jorilex;

public class Producte {
	private int id;
	private int quantitat;
	private String nomProducte;
	
	Producte(int id, int quantitat){
		this.id = id;
		this.quantitat = quantitat;
		this.nomProducte = "";
	}
	
	public int getId() {
		return id;
	}

	public String getNomProducte() {
		return nomProducte;
	}

	public void setNomProducte(String nomProducte) {
		this.nomProducte = nomProducte;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQuantitat() {
		return quantitat;
	}

	public void setQuantitat(int quantitat) {
		this.quantitat = quantitat;
	}
}
