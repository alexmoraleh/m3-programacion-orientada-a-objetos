package moodle;

public class Grup {
	private String nomGrup;
	private String descripcio;
	private boolean obert;
	
	Grup (String n, String d, boolean o){
		nomGrup = n;
		descripcio = d;
		obert = o;
	}
	
	public void setNom (String n){
		nomGrup = n;
	}
	public void setDescripcio (String d){
		descripcio = d;
	}
	
	public String getNomGrup () {
		return (nomGrup);
	}
	public String getDescripcio () {
		return (descripcio);
	}
	
	public void SetObert(boolean o) {
		obert = o;
	}
	
	public boolean getObert() {
		return obert;
	}
	
	public String veureGrup (){
		String ast = (obert)?"*":"";
		return (this.nomGrup + " (" + descripcio + ") " + ast + "\n" );
	}

	@Override
	public String toString() {
		return "Nom: " + nomGrup;
	}

	
}
