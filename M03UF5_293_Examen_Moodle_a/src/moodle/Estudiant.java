package moodle;

public class Estudiant {
	private String nom;
	private String cognom1;
	private String cognom2;
	private String usuari;
	private boolean baixa;

	public Estudiant(String n, String c1, String c2, String u, boolean b) {
		nom = n;
		cognom1 = c1;
		cognom2 = c2;
		usuari = u;
		baixa = b;
	}

	public String getNom() {
		return nom;
	};

	public String getCognom1() {
		return cognom1;
	}

	public String getCognom2() {
		return cognom2;
	}
	
	public String getUsuari() {
		return usuari;
	}

	public void setNom(String nom) {
		this.nom = nom;
	};

	public void setCognom1(String c1) {
		this.cognom1 = c1;
	};
	public void setCognom2(String c2) {
		this.cognom2 = c2;
	};
	public void setUsuari(String usuari) {
		this.usuari = usuari;
	};
	public void setBaixa(boolean b) {
		this.baixa = b;
	};
	public boolean getBaixa() {
		return baixa;
	};
	public String veureEstudiant () {
		String esbaixa = (baixa)?"(baixa)":"";
		return (usuari + "..." + cognom1 + " " + cognom2 + ", " + nom +" "+ esbaixa +"\n");
	}
		
}
