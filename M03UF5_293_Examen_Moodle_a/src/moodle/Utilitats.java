package moodle;

import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JList;

public class Utilitats {
	static public void generarLlistaEstudiants(List<Estudiant> llista) {
		Estudiant e1=new Estudiant("Anna","Abad","Anton","AAA", true);
		llista.add(e1);
		e1=new Estudiant("Berta","Bou","Briones","BBB", false);
		llista.add(e1);
		e1=new Estudiant("Carles","Cal","Costelo","CCC", false);
		llista.add(e1);
		e1=new Estudiant("Denis","Doltz","Duarte","DDD",false);
		llista.add(e1);
		e1=new Estudiant("Ernest","Esp�","Eixart","EEE", true);
		llista.add(e1);
		e1=new Estudiant("Flora","Font","Florit","FFF", false);
		llista.add(e1);
		e1=new Estudiant("Gerard","Gim","Gomez","GGG", false);
		llista.add(e1);
		e1=new Estudiant("Hip�lit","Hern�ndez","Huerta","HHH", false);
		llista.add(e1);
		e1=new Estudiant("Ignasi","Ivart","Iba�ez","III", true);
		llista.add(e1);
		e1=new Estudiant("Jacinto","Jover","Juncal","JJJ", false);
		llista.add(e1);
		e1=new Estudiant("Karina","Kurt","Kandajar","KKK", false);
		llista.add(e1);
		e1=new Estudiant("Leandro","Lupez","Lopez","LLL", false);
		llista.add(e1);
		e1=new Estudiant("Mar","Maroto","Moya","MMM", false);
		llista.add(e1);
		e1=new Estudiant("Narc�s","Nogal","Naves","NNN", false);
		llista.add(e1);
		e1=new Estudiant("Ot","Oliveres","Ortega","OOO", true);
		llista.add(e1);
		e1=new Estudiant("Paula","Pi","Pou","PPP", false);
		llista.add(e1);
		e1=new Estudiant("Quint�","Quer","Quiron","QQQ", false);
		llista.add(e1);
		e1=new Estudiant("Ramon","Rios","Ruiz","RRR", false);
		llista.add(e1);
		e1=new Estudiant("Sandra","Suarez","Soria","SSS", false);
		llista.add(e1);
		e1=new Estudiant("Tania","Tebas","Tort","TTT", false);
		llista.add(e1);
		e1=new Estudiant("Unai","Unquera","Usas","UUU", true);
		llista.add(e1);
		e1=new Estudiant("Vanessa","Vinya","Vitolo","VVV", false);
		llista.add(e1);
		e1=new Estudiant("X�nia","Xip","Xop","XXX", false);
		llista.add(e1);
		e1=new Estudiant("Zoe","Zapata","Zuviria","ZZZ", false);
		llista.add(e1);
	}
	
	static public void generarLlistaGrups(List<Grup> llista) {
		Grup g = new Grup ("Angl�s", "English!!!", false);
		llista.add(g);
		g = new Grup ("Biologia", "Biologia!!!", false);
		llista.add(g);
		g = new Grup ("Computadors", "Computadors!!!", true);
		llista.add(g);
		g = new Grup ("Desenvolupament", "DAM!!!", true);
		llista.add(g);
		g = new Grup ("Electr�nica", "Electr�nica!!!", false);
		llista.add(g);
		g = new Grup ("M�bils", "Android", true);
		llista.add(g);
		g = new Grup ("MongoDB", "Base de Dades", true);
		llista.add(g);
	}	

}
