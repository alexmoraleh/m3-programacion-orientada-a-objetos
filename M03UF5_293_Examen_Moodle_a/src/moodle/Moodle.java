package moodle;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

public class Moodle {
	private Map<Grup, Set <Estudiant>> curs = new HashMap<Grup, Set <Estudiant>>();

	Moodle(List <Grup> lg){
		for(Grup g:lg) {
			curs.put(g, new HashSet<Estudiant>());
		}
	}

	//TODO  implementar m�tode afegirEstudiant ...
	public void afegirEstudiant(Grup g, Set<Estudiant> s) {
		curs.put(g, s);
	}
	
	//TODO  implementar m�tode veureEstudiantsGrup ...
	public Set<Estudiant> veureEstudiantsGrup(Grup g) {
		return curs.get(g);
	}
	
	//TODO  implementar m�tode VeureGrupsEstudiant ...
	public void VeureGrupsEstudiant() {
		
	}
	public Collection<Set<Estudiant>> veureEstudiants() {
		return curs.values();
	}

	@Override
	public String toString() {
		String s=null;
		for(Grup g:curs.keySet()) {
			s=s+g.toString()+"\n";
		}
		return s;
	}
	


}
